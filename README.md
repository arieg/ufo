Welcome to UFO, the home of Whale and Vinta projects.

## License

The license is in LICENSE.TXT.

## Compilation Instructions

See [http://bitbucket.org/arieg/ufo/wiki/develop/install](http://bitbucket.org/arieg/ufo/wiki/develop/install)

## Compilation with Cmake

1. Compile LLVM using cmake

          mkdir build-llvm ; cd build      
          cmake -DCMAKE_BUILD_TYPE:STRING=Release -DCMAKE_INSTALL_PREFIX:PATH=/ag/llvm/run/2.9 -DLLVM_TARGETS_TO_BUILD:STRING=X86 /ag/llvm/src/2.9/ 
          make
          make install

2. Configure UFO using

        mkdir build ; cd build
        cmake <PATH-TO-SRC> -DCMAKE_BUILD_TYPE=Debug -DLLVM_CONFIG_EXECUTABLE=/ag/llvm/run/2.9-cmake/bin/llvm-config -DZ3_ROOT=/ag/z3/run/unstable -DMATHSAT_ROOT=/ag/mathsat/run/5.2.6/ -DLDD_ROOT=/ag/git/ldd -DAPRON_ROOT=/ag/apron/run/svn/ -DABC_ROOT=/ag/abc/src/ag/

Everything after `Z3_ROOT` is optional. Other available options can be
seen with `cmake -LH` or by using one of cmake GUI tools `ccmake` or
`cmake-gui`.

To compile only the UFO executable, do  `make ufo`.


## Running

A sample command line is

    time ./tools/bin/ufo -lawi -ufo-increfine=REF4 --ufo-post=BOXES --ufo-widen-step=1 --ufo-simplify=false --ufo-consrefine=true --ufo-conjoin=false --ufo-false-edges=true --ufo-cover=GLOBAL -ufo-dvo=true svb/obj/tacas2012/token_ring.01.cil.c.ufo.co.bc

But, it is easier to use the [UFO front-end](http://bitbucket.org/arieg/ufofe)
