#include "ufo/Smt/ForallSimplify.hpp"
#include "ufo/Smt/ExprZ3.hpp"

using namespace ufoz3;

namespace ufo
{

  namespace
  {
    /** A filter that returns true for a non-Boolean term that
	contains a given variable */
    struct TermFilter
    {
      Expr var;
      TermFilter (Expr v) : var (v) {}
      
      bool operator() (Expr v)
      {
	// -- all non-boolean terms that contain var
	return !isOp<BoolOp> (v) && contains (v, var);
      }
    };


    /** A function to protect variants from being replaced */
    template <typename M>
    struct ProtectVariantsFn 
    {
      typedef typename M::const_iterator const_iterator;

      const M& map;
      ProtectVariantsFn (const M &m) : map (m) {}

      Expr operator() (Expr k) const
      {
	const_iterator it = map.find (k);
	if (it == map.end ())
	  {
	    // -- replace k by k to avoid descending into it
	    if (isOpX<VARIANT> (k)) return k;
	    
	    // -- NULL
	    return Expr ();
	  }
	
	return it->second;
      }
      
    };
  }


  /** Quantifier elimination using Z3 */
  Expr forallElim (Expr e, ExprSet &vars)
  {
    Expr res = e; //z3_simplify (e); 
    foreach (Expr v, vars)
      {
	{
	  ExprSet vv;
	  vv.insert (v);
	  (errs () << "Eliminating " << *v << "\n").flush ();
	  TermFilter tf(v);
	  ExprVector terms;
	  filter (res, tf, std::back_inserter (terms));
	  foreach (Expr t, terms) errs () << "\tTERM: " << *t << "\n";

	  errs () << "\tsz before elim " << dagSize (res);
	  res = z3_forall_elim (res, vv);
	  errs () << " sz after elim " << dagSize (res);
	  //res = z3_lite_simplify (res);
	  (errs () << " sz after simplify " << dagSize (res) << "\n").flush ();

	}
      }

    return res;
  }

  namespace 
  {
    struct ForallElimTest
    {
      Expr v;
    
      bool success;
    
      boost::shared_ptr<boolop::TrivialSimplifier> r;

      typedef boost::unordered_map<Expr,bool> ExprBoolMap;
      ExprBoolMap cache;
    
      ForallElimTest (Expr var) : 
	v(var), success (true), 
	r (new boolop::TrivialSimplifier (var->efac ())) 
      {}
    

    
      /** true if e contains v as a sub-expression */
      bool hasV (Expr e)
      {
	// -- strip negation
	if (isOpX<NEG> (e)) e = e->left ();

	if (v == e) return true;
	// -- don't cache terminals
	if (e->arity () == 1) return false;


	// -- don't descent into variants or assumptions
	if (isOpX<VARIANT> (e) || isOpX<ASM> (e)) return false;

	// -- only cache Boolean operators to avoid polutting the cache
	if (e->use_count () > 1 && isOp<BoolOp> (e))
	  {
	    ExprBoolMap::const_iterator it = cache.find (e);
	    if (it != cache.end ()) return it->second;
	  }

	bool res = false;
	foreach (Expr arg, make_pair(e->args_begin (), e->args_end ()))
	  {
	    res |= hasV (arg);
	    if (res) break;
	  }

	if (e->use_count () > 1 && isOp<BoolOp> (e)) cache[e] = res;
	return res;
      }
    

      VisitAction operator() (Expr exp)
      {
	if (isOpX<AND> (exp)) 
	  return VisitAction::changeDoKidsRewrite (exp, r);
      
	// -- assume NNF, so negation is the lowest Boolean operator
	if (isOpX<NEG> (exp))
	  {
	    if (hasV (exp->left ()))
	      return VisitAction::changeTo (r->falseE);
	    return VisitAction::skipKids ();
	  }
      
	if (isOpX<OR> (exp))
	  {
	    size_t count = 0;
	    // -- check if there is only one disjunct that contains v if
	    // -- so, eliminate v from that disjunct if not, mark that
	    // -- elimination was not successful and back out of this
	    // -- branch
	    foreach (Expr a, make_pair (exp->args_begin (), exp->args_end ()))
	      if (hasV (a) && count++ != 0) 
		{
		  success = false;
		  break;
		}
	  
	    if (count <= 1)
	      return VisitAction::changeDoKidsRewrite (exp, r);
	    else
	      return VisitAction::skipKids ();

	  }
      
	// -- an atom, if has v becomes FALSE
	if (hasV (exp)) return VisitAction::changeTo (r->falseE);
	// -- an atom, if has no v is unchanged
	return VisitAction::skipKids ();
      }
    };
  }
  
  /** Partial quantifier elimination by pushing the quantifier to terms*/
  Expr forallSimplify (Expr e, ExprSet &vars)
  {
    Expr res = e;
    for (ExprSet::iterator it = vars.begin (), end = vars.end (); 
	 it != end; )
      {
	Expr v = *it;
	ForallElimTest fet (v);
	res = dagVisit (fet, res);
	if (fet.success) 
	  {
	    vars.erase (it++);
	    errs () << "forallSimplify: removed: " << *v << "\n";
	    errs ().flush ();
	  }
	else 
	  {
	    ++it;
	    errs () << "forall simplify failed on: " << *v << "\n";
	  }
	
      }
    return res;
  }
  
  
  /** Quantifier elimination heuristic that ties everything together */
  Expr forallHeuristic (Expr e, ExprSet &vars)
  {
    ExprFactory &efac = e->efac ();

    e = forallSimplify (boolop::nnf (e), vars);

    if (vars.empty ())
      {
	errs () << "forallSimplifyDidTheJob\n";
	errs ().flush ();
	return e;
      }
    

    Expr falseE = mk<FALSE> (efac);
    mpq_class zero(0);
    Expr zeroE = mkTerm (zero, efac);

    ExprMap map;
    foreach (Expr v, vars)
      {
	Expr u = v;
	
	// -- strip up to two levels of variants to get to the
	// -- underlying value
	if (isOpX<VARIANT> (u)) u = variant::mainVariant (u);
	if (isOpX<VARIANT> (u)) u = variant::mainVariant (u);
	const Value *val = getTerm<const Value*> (u);
	assert (val != NULL);

	if (isBoolType (val->getType ())) map[v] = falseE;
	else map[v] = zeroE;

	errs () << "\tEVAR: " << *v << "\n";
      }

    ExprZ3 z3(efac, StringMap (), true);
    
    Expr last = mk<TRUE>(efac);
    ExprVector res;
    
    z3.assertExpr (mk<NEG>(e));
    z3.push ();
    int count = 0;
    int max_count = 3;
    ExprSet skipped;
    while (!vars.empty () && z3.solve () != false)
      {
	// -- if takes too long, give up
	if (++count > max_count) 
	  {
	    Expr v = *(vars.begin ());
	    skipped.insert (v);
	    vars.erase (vars.begin ());
	    map.erase (v);
	    
	    // -- reset state
	    res.clear ();
	    z3.pop ();
	    z3.push ();
	    errs () << "\tSkip: " << *v << "\n";
	    
	    max_count += max_count;
	    if (vars.empty ()) break;
	  }
	
	
	//z3.debugPrintModel ();
	// - update map
	foreach (Expr v, vars)
	  {
	    Expr mv = z3.getModelValue (v);
	    if (mv == v || isOpX<NONDET> (mv)) continue;	    
	    map[v] = mv;
	    errs () << "Updated " << *v << " to " << *mv << "\n";
	    errs ().flush ();
	  }
	last = replaceSimplify (e, 
				mk_fn_map (ProtectVariantsFn<ExprMap> (map)));
	z3.assertExpr (last);

	// -- add last to the result
	res.push_back (last);
	errs () << "forall-heuristic: count: " << count << "\n";
      }
    
    // -- check if last is good enough
    z3.pop ();
    
    if (!vars.empty ())
      {
	z3.assertExpr (last);
	if (z3.solve () == false)
	  {
	    errs () << "LAST WORKED\n";
	    res.clear ();
	    res.push_back (last);
	  }
	else 
	  errs () << "LAST IS NOT GOOD ENOUGH\n";
      }
    else 
      {
	assert (!skipped.empty ());
	res.push_back (e);
      }

    // -- apply z3 for anything we did not remove
    if (!skipped.empty ()) 
      {
	ExprVector in = res;
	res.clear ();
	foreach (Expr inE, in)
	  res.push_back (forallElim (inE, skipped));
      }
    
    return mknary<AND> (mk<TRUE>(efac), res.begin (), res.end ());
  }  
}


