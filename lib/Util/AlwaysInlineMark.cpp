#include "ufo/InitializePasses.h"

#include "llvm/Pass.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/Instructions.h"
#include "llvm/Function.h"
#include "llvm/BasicBlock.h"
#include "llvm/PassManager.h"
#include <llvm/LLVMContext.h>
#include "llvm/Support/IRBuilder.h"


using namespace llvm;

namespace 
{
  /** Replaces return statements with an infinite loop */
  struct AlwaysInlineMark : public FunctionPass
  {

    static char ID;
    AlwaysInlineMark() : FunctionPass (ID) {}

    virtual bool runOnFunction(Function &F) 
    {
      // -- skip main function
      if (F.getName ().compare ("main") == 0) return false;

      // -- skip declarations
      if (F.isDeclaration ()) return false;

      // -- add the attribute
      F.addFnAttr (llvm::Attribute::AlwaysInline);
      return true;
    }
    
    virtual void getAnalysisUsage (AnalysisUsage &AU) const
    {
      AU.setPreservesAll ();
    }
    
  };
char AlwaysInlineMark::ID = 0;
}

INITIALIZE_PASS(AlwaysInlineMark, "always-inline-mark", 
		"marks all functiosn to be always inlined", false, false)
