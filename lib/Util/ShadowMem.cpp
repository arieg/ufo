#include "ufo/InitializePasses.h"

#include "llvm/Pass.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/Instructions.h"
#include "llvm/Function.h"
#include "llvm/Module.h"
#include "llvm/Support/Compiler.h"
#include "llvm/PassManager.h"

#include "llvm/Support/IRBuilder.h"

using namespace llvm;

namespace 
{
  /** A pass to shadow memory accesses (load/store) with function
      calls to implicitly cast memory accesses to SSA */
  struct ShadowMem : public ModulePass 
  {
    Constant *memLoadFn;
    Constant *memStoreFn;

    static char ID;
    ShadowMem() : ModulePass(ID) {}

    virtual bool runOnModule(Module &M) 
    {
      LLVMContext &Context = M.getContext ();

      // -- create our marker function
      memLoadFn = M.getOrInsertFunction ("whale.mem.load", 
					 Type::getVoidTy(Context),
					 Type::getInt32Ty (Context),
					 (Type*) 0);
      memStoreFn = M.getOrInsertFunction ("whale.mem.store", 
					 Type::getInt32Ty (Context),
					 Type::getInt32Ty (Context),
					 (Type*) 0);

				      
      //Iterate over all functions, basic blocks and instructions.
      for (Module::iterator FI = M.begin(), E = M.end(); FI != E; ++FI)
	runOnFunction (*FI);
      return true;
    }

    virtual bool runOnFunction (Function &F)
    {
      // -- ignore functions without bodies
      if (F.isDeclaration ()) return false;

      LLVMContext& Context = F.getContext ();
      IRBuilder<> B (Context);

      // -- create alloca for whale.mem. It will be wired into the
      // -- code later
      AllocaInst *whaleMem = new AllocaInst (Type::getInt32Ty (Context), 0);

      for (Function::iterator b = F.begin(), be = F.end(); b != be; ++b)
	for (BasicBlock::iterator i = b->begin(), ie = b->end(); i != ie; ++i) 
	  {
	    // -- prefix load inst with a use of whale.mem
	    if (isa<LoadInst> (i))
	      {
		B.SetInsertPoint (i);
		B.CreateCall (memLoadFn, B.CreateLoad (whaleMem));
	      }
	    // -- prefix a store inst with a use and new def of whale.mem
	    else if (isa<StoreInst> (i))
	      {
		B.SetInsertPoint (i);
		B.CreateStore 
		  (B.CreateCall (memStoreFn, B.CreateLoad (whaleMem)), 
		   whaleMem);
	      }
	  }

      // -- wire allocation of whale.mem as first instruction of
      // -- function entry point
      B.SetInsertPoint (&*F.getEntryBlock ().begin ());
      B.Insert (whaleMem, "whale.mem");
      B.CreateStore (B.getInt32 (0), whaleMem);
      
      return true;
    }
    

    virtual void getAnalysisUsage (AnalysisUsage &AU) const
    {
      AU.setPreservesAll ();
    }    
  };
char ShadowMem::ID = 0;
}

//static RegisterPass<ShadowMem> X("shadow-mem", 
//				      "Shadows memory with whale calls");
INITIALIZE_PASS(ShadowMem, "shadow-mem", 
		"Shadows memory with whale calls", false, false)
