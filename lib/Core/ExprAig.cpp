#include "ufo/ufo.hpp"
#include "ufo/ExprAig.hpp"
#include "boost/range/algorithm_ext/for_each.hpp"
#include "boost/range/algorithm/sort.hpp"
#include "boost/shared_ptr.hpp"

using namespace expr;

namespace
{

  inline bool isNeg (const Expr &a)
  { return isOpX<NEG> (a) || isOpX<NEG_G> (a); }
  

  inline bool isNum (const Expr &a)
  { return isOpX<MPZ> (a) || isOpX<MPQ> (a) || isOpX<INT> (a); }

  inline mpq_class getNum (const Expr &a)
  {
    mpq_class res;
    if (isOpX<MPQ> (a))
      res = getTerm<mpq_class> (a);
    else if (isOpX<MPZ> (a))
      res = getTerm<mpz_class> (a);
    else if (isOpX<INT> (a))
      res = getTerm<int> (a);
    else
      assert (0 && "UNREACHABLE");
    
    return res;
  }
  

  /// orient the operator so that the constant is on the right
  inline Expr orient (const Expr &a) 
  {
    assert (a->arity () == 2);

    if (!isNum (a->left ())) return a;
      
    Expr res;
    if (isOpX<EQ> (a)) res = mk<EQ> (a->right (), a->left ());
    else if (isOpX<NEQ> (a)) res = mk<NEQ> (a->right (), a->left ());
    else if (isOpX<LEQ> (a)) res = mk<GEQ> (a->right (), a->left ());
    else if (isOpX<LT> (a)) res = mk<GT> (a->right (), a->left ());
    else if (isOpX<GT> (a)) res = mk<LT> (a->right (), a->left ());
    else if (isOpX<GEQ> (a)) res = mk<LEQ> (a->right (), a->left ());
      
    assert (res);
    assert (!isNum (res->left ()));
    return res;
  }
    
  inline Expr lneg (const Expr &a) 
  {
    Expr res;
    if (isOpX<EQ> (a)) res = mk<NEQ> (a->left (), a->right ());
    else if (isOpX<NEQ> (a)) res = mk<EQ> (a->left (), a->right ());
    else if (isOpX<LEQ> (a)) res = mk<GT> (a->left (), a->right ());
    else if (isOpX<LT> (a)) res = mk<GEQ> (a->left (), a->right ());
    else if (isOpX<GT> (a)) res = mk<LEQ> (a->left (), a->right ());
    else if (isOpX<GEQ> (a)) res = mk<LT> (a->left (), a->right ());      

    assert (res);
    return res;
  }
    
  inline Expr labs (const Expr &a) 
  { return isNeg (a) ? lneg (a->left ()) : a; }
    

  inline bool isCmp (const Expr &a)
  {
    return isNeg (a) ?  
      isOp<ComparissonOp> (a->left ()) : isOp<ComparissonOp> (a);
  }
  
    
  /// evaluate a comparison operator whose both arguments are numeric
  inline bool evalCmp (const Expr &exp)
  {
    mpq_class lNum (getNum (exp->left ()));
    mpq_class rNum (getNum (exp->right ()));
    
    if (isOpX<EQ> (exp)) return lNum == rNum;
    if (isOpX<NEQ> (exp)) return lNum != rNum;
    if (isOpX<LT> (exp)) return lNum < rNum;
    if (isOpX<LEQ> (exp)) return lNum <= rNum;
    if (isOpX<GT> (exp)) return lNum > rNum;
    if (isOpX<GEQ> (exp)) return lNum >= rNum;

    assert (0 && "UNREACHABLE");
    exit (1);
    return false;
  }
  
  template <typename ForwardIterator>
  ForwardIterator reduce (ForwardIterator first, ForwardIterator last)
  {
    if (first == last) return last;
    
    ForwardIterator begin = first;
    
    ForwardIterator result = first;

    // skip TRUE
    while (first != last && isOpX<TRUE> (*first)) ++first;

    if (first == last) return last;

    if (! isCmp (*first))
      {
	*result = *first;
	while (++first != last && ! isCmp (*first))
	  {
	    if (*result == *first) continue;
	    // -- contradiction
	    if (isNeg (*first) && *result == (*first)->left ()) return begin;
	    *(++result) = *first;
	  }
	++result;
      }

    if (first == last) return last;

    // -- deal with non-compare operators
    assert (isCmp (*first));

    *result = *first;
    Expr ord = orient (labs (*first));
    mpq_class oNum (0);
    
    if (!isNum (ord->right ())) ord.reset ();
    else oNum = getTerm<mpq_class> (ord->right ());

    while (++first != last)
      {
	if (*result == *first) continue;
	
	Expr cur = orient (labs (*first));
	mpq_class cNum (0);
	
	// -- normalized versions equal
	if (ord && cur == ord) continue;
	
	if (!isNum (cur->right ()))
	  {
	    if (isNeg (*first) && *result == (*first)->left ()) return begin;
	    *(++result) = *first;
	    ord.reset ();
	    continue;
	  }

	cNum = getTerm<mpq_class> (cur->right ());
	if (isNum (cur->left ()))
	  { if (evalCmp (cur)) continue; else return begin; }

	if (ord && ord->left () == cur->left ())
	  {
	    
	    if (isOpX<EQ> (ord))
	      {
		// -- contradiction t=v && t=u && v!=u
		if (isOpX<EQ> (cur))
		  {
		    assert (oNum != cNum);
		    return begin;
		  }
		// -- contradiction t=v && t!=v
		else if (isOpX<NEQ> (cur) && ord->right () == cur->right ())
		  return begin;

		// skip NEQ since we already have an EQ that is stronger
		continue;
	      }
	    
	    
	    if ((isOpX<GT> (ord) || isOpX<GEQ> (ord) || isOpX<EQ> (ord)) &&
		(isOpX<GT> (cur) || isOpX<GEQ> (cur))) 
	      {
		if (isOpX<GT> (cur) && ! (oNum > cNum)) 
		  {
		    errs () << "shortcut1: " << oNum << " > " << cNum << "\n";
		    return begin;
		  }
		
		if (isOpX<GEQ> (cur) && ! (oNum >= cNum)) 
		  {
		    errs () << "shortcut2: " << oNum << " >= " << cNum << "\n";
		    return begin;
		  }

		continue;
	      }
	    
	    else if ((isOpX<GT> (ord) || isOpX<GEQ> (ord)) &&
		     (isOpX<LT> (cur) || isOpX<LEQ> (cur)))
	      {
		if (isOpX<LEQ> (cur) && isOpX<GEQ> (ord) && 
		    ! (oNum <= cNum)) return begin;
		if ((isOpX<LT> (cur) || isOpX<GT> (ord)) && 
		    ! (oNum < cNum)) return begin;
		
		// -- otherwise, cur is current upper bound, keep it
	      }
	    else if ((isOpX<LT> (ord) || isOpX<LEQ> (ord) || isOpX<EQ> (ord)) &&
		(isOpX<LT> (cur) || isOpX<LEQ> (cur))) 
	      {
		if (isOpX<LT> (cur) && ! (oNum < cNum)) return begin;
		if (isOpX<LEQ> (cur) && ! (oNum <= cNum)) return begin;
		continue;
	      }
	    
	  }

	// default case
	*(++result) = *first;
	ord = cur;
	oNum = cNum;
	
      }
    
    return ++result;
    
  }

  

  struct Negate : public std::binary_function<Expr&,Expr,void>
  { void operator() (Expr &s, Expr t) const { s = gate::lneg (t);} };
  
  struct SimplifyLessThan : public std::binary_function<const Expr&,
							const Expr&,bool>
  { 
    bool operator () (const Expr &a, const Expr &b) const 
    { 
      if (a == b) return false;
      
      if (isOpX<TRUE> (a)) return true;
      if (isOpX<TRUE> (b)) return false;
      
      assert (!isOpX<FALSE> (a) && !isOpX<FALSE> (b));
      
      Expr A;
      Expr B;

      A = isNeg (a) ? a->left () : a;
      B = isNeg (b) ? b->left () : b;

      if (A == B && !isOp<ComparissonOp> (A))
	// -- one argument is a negation of the other
	return A == a ? true : false;

      // -- terminals
      if (A->arity () == 0 && B->arity () == 0) return A < B;      
      if (A->arity () == 0) return true;
      if (B->arity () == 0) return false;
      
      // -- variables
      if (isOpX<BIND> (A) && isOpX<BIND> (B)) return A < B;      
      if (isOpX<BIND> (A)) return true;
      if (isOpX<BIND> (B)) return false;

      // -- predicates
      if (isOpX<FAPP> (A) && isOpX<FAPP> (B)) return A < B;
      if (isOpX<FAPP> (A)) return true;
      if (isOpX<FAPP> (B)) return false;


      if (A->arity () != 2 && B->arity () != 2) return A < B;
      if (A->arity () != 2) return true;
      if (B->arity () != 2) return false;

      // -- arity is 2

      if (! (isOp<ComparissonOp> (A) && isOp<ComparissonOp> (B) ))
	{
	  if (isOp<ComparissonOp> (A)) return false;
	  if (isOp<ComparissonOp> (B)) return true;
	  assert (! (A == B));
	  return A < B;
	}
      // -- both are comparison ops


      // -- all comparison ops are at the end of the order

      A = orient (labs (a));
      B = orient (labs (b));

      if (A == B) return false;
      
      if (isNum (A->right ()) && isNum (B->right ()))
	{
	  if (! (A->left () == B->left ())) return A->left () < B->left ();
	}
      else if (!isNum (A->right ()) && !isNum (B->right ())) 
	return A->left () == B->left () ? 
	  A->right () < B->right () : A->left () < B->left ();      
      else // at least one of A->right() and B->right() is a number
	return !isNum (A->right ());

      // -- A->left == B->left  and isNum (A->right ()) and isNum (B->right ())
      
      assert (isNum (A->right ()));
      assert (isNum (B->right ()));
      mpq_class aNum (getNum (A->right ()));
      mpq_class bNum (getNum (B->right ()));

      // -- comparison op

      // -- A is equality, if B is equality, order by numeric part
      if (isOpX<EQ> (A))
	return isOpX<EQ> (B) ? aNum < bNum : true;
      if (isOpX<EQ> (B)) return false;

      if (isOpX<NEQ> (A)) 
	return isOpX<NEQ> (B) ? aNum < bNum : true;
      if (isOpX<NEQ> (B)) return false;
      
      if (isOpX<GT> (A) || isOpX<GEQ> (A))
	{
	  if (isOpX<GT> (B) || isOpX<GEQ> (B))
	    return aNum == bNum ? isOpX<GT> (A) : aNum > bNum;
	  return true;
	}
      
      if (isOpX<GT> (B) || isOpX<GEQ> (B)) return false;
      
      if (isOpX<LT> (A) || isOpX<LEQ> (A))
	{
	  if (isOpX<LT> (B) || isOpX<LEQ> (B))
	    return aNum == bNum ? isOpX<LT> (A) : aNum < bNum;
	  return true;
	}
      
      if (isOpX<LT> (B) || isOpX<LEQ> (B)) return false;
      
      dbgs () << "Failed to compare " << *A << " < " << *B << "\n";
      assert (0 && "UNREACHABLE");
      return A < B;
    } 

    bool termLT (const Expr &a, const Expr &b) const
    {
      if (a->left () == b->left ()) return a->right () < b->right ();
      return a->left () < b->left ();
    }
  
  };

  inline Expr strash (Expr e)
  {
    if (isOpX<AND_G> (e)) return mknary<AND> (e->args_begin (), e->args_end ());
    if (isOpX<NEG_G> (e)) return mk<NEG> (e->left ());
    return e;
  }
    

  inline bool isAnd (const Expr &e) { return isOpX<AND> (e) || isOpX<AND_G> (e); }
  inline Expr flat_and (const Expr &e)
  {
    if (!isAnd (e)) return e;
    if (isOpX<AND> (e) && e->arity () == 2) return e;
    
    Expr res;
    rev_forall (Expr k, make_pair (e->args_begin (), e->args_end ()))
      res = res ? mk<AND> (k, res) : k;
    return res;
  }

  struct Flatten : public std::unary_function<Expr,Expr>
  {
    ExprFactory &m_efac;
    bool m_gather;
    
    Expr trueE;
    Expr falseE;

    
    Flatten (ExprFactory &efac, bool gather = true) : 
      m_efac (efac), m_gather (gather),
      trueE (mk<TRUE> (m_efac)), falseE (mk<FALSE> (m_efac)) {}

    
      
    Expr operator() (Expr exp)
    {
      if (exp->arity () == 0) return exp;


      if (isOpX<OUT_G> (exp) || isNeg (exp))
	{
	  Expr res = exp->left ();
	  
	  if (!isAnd (res)) res = strash (res);
	  else res = m_gather ? strash (res) : flat_and (res);
	  
	  if (isOpX<OUT_G> (exp)) return mk<OUT_G> (res);
	  return boolop::lneg (res);
	}      
      
      if (!isAnd (exp)) return exp;

      return reduce_and (exp);
    }

    Expr reduce_and (const Expr &exp)
    {      
      assert (isAnd (exp));
      
      ExprVector kids;
      // -- guess at how much space we need
      kids.reserve (exp->arity ());

      if (collect_args (make_pair(exp->args_begin (), exp->args_end ()),
			std::back_inserter (kids))) return falseE;

      boost::sort (kids, SimplifyLessThan());

      // -- unique
      kids.resize (std::distance (kids.begin (), 
				  reduce (kids.begin (), kids.end ())));
      
      return mkNew<AND_G> (exp, kids);
    }

    template <typename OpType>
    Expr mkNew (const Expr &exp, const ExprVector &kids)
    {
      if (kids.size () == 0) return falseE;
      if (kids.size () == 1) return kids [0];

      // -- no change
      if (kids.size () == exp->arity () &&
	  std::equal (exp->args_begin (), exp->args_end (), kids.begin ()))
	return exp;
      
      return mknary<OpType> (kids.begin (), kids.end ());
    }
    
    

    /// -- collects all arguments, returns true if FALSE was seen
    template <typename Range, typename OutputIterator>
    bool collect_args (const Range &rng, OutputIterator out)
    {
      forall (Expr a, rng)
	{
	  if (isOpX<FALSE> (a)) return true;
	  if (isAnd (a)) std::copy (a->args_begin (), a->args_end (), out);
	  else *(out++) = a;
	}
      return false;
    }
  };


    
  struct FlatAig
  {      
    
    ExprFactory &m_efac;
    boost::shared_ptr<Flatten> m_flat;

    Negate m_negate;
    

    FlatAig (ExprFactory &efac, bool gather = true) : 
      m_efac (efac), m_flat (new Flatten (m_efac, gather)) { }
    
    VisitAction operator() (Expr exp)
    {
      // -- constants
      if (isOpX<TRUE> (exp) || isOpX<FALSE> (exp))
	return VisitAction::skipKids ();
      if (exp->arity () == 0) return VisitAction::skipKids ();

      if (isOpX<OUT_G> (exp)) return VisitAction::changeDoKidsRewrite (exp, m_flat);

      // -- true if we are under negation
      bool negated = false;
      if (isNeg (exp)) 
	{
	  // -- strip one level of negation
	  if (isNeg (exp->left ()))
	    {
	      exp = exp->left ()->left ();
	      // -- if there are more levels of negation, continue to visit kids
	      if (isNeg (exp)) return VisitAction::changeDoKidsRewrite (exp, m_flat);

	      // -- if exp is not negation, we continue with other
	      // -- branches and possibly other rewrite-rules
	    }
	  else if (isOpX<OR> (exp->left ()))
	    {
	      exp = exp->left ();
	      negated = true;
	    }
	  
	  else return VisitAction::changeDoKidsRewrite (exp, m_flat);
	}
      
      if (isAnd (exp)) return VisitAction::changeDoKidsRewrite (exp, m_flat);
      
      if (isOpX<OR> (exp)) 
	{
	  assert (exp->arity () >= 1);
	  ExprVector kids (exp->arity ());
	  
	  boost::for_each (kids, make_pair (exp->args_begin (), 
					    exp->args_end ()), m_negate);
	  
	  Expr res = mknary<AND_G> (kids);
	  if (!negated) res = mk<NEG_G> (res);
      
	  return VisitAction::changeDoKidsRewrite (res, m_flat);
	}

      return VisitAction::skipKids ();
    }      
    
  };
}

Expr expr::boolop::aig (Expr e, bool gather)
{
  FlatAig faig (e->efac (), gather);
  Expr res = dagVisit (faig, mk<OUT_G> (e));
  
  assert (isOpX<OUT_G> (res));
  return res->left ();
}

namespace
{
  struct AigSize 
  {
    unsigned gates;
    unsigned inputs;
    
    AigSize () : gates (0), inputs (0) {}
    
    VisitAction operator() (Expr e) 
    {
      if (isOpX<AND> (e)) gates++;
      else if (!isOpX<NEG> (e))  
	{
	  inputs++;
	  return VisitAction::skipKids ();
	}
      
      return VisitAction::doKids ();
    }    
    unsigned size () const { return gates + inputs; }
    unsigned gateSz () const { return gates; }
    unsigned inputSz () const { return inputs; }
  };
}



unsigned expr::boolop::aigSize (Expr e)
{
  AigSize sz;
  dagVisit (sz, e);
  return sz.size ();
}
