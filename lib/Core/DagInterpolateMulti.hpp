#ifndef __DAG_INTERPOLATE_MULTI__H_
#define __DAG_INTERPOLATE_MULTI__H_

/** DAG interpolation with multiple SAT calls. Unfinished. */

#include <boost/range.hpp>
#include <boost/iterator/zip_iterator.hpp>
#include <boost/tuple/tuple.hpp>
#include <boost/graph/graph_traits.hpp>
#include <boost/property_map/property_map.hpp>

#include "ufo/ufo.hpp"
#include "ufo/Expr.hpp"

namespace ufo
{
  /** Dag Interpolation Procedure 
   *
   * \tparam Graph boost::Graph from BGL
   * \tparam ItpSmt an interpolating SMT solver
   */
  template <typename Graph, typename ItpSmt>
  class DagInterpolateMulti
  {
  public:
    typedef graph_traits<Graph> Traits;
    typedef typename Traits::vertex_descriptor Vertex;
    typedef typename Traits::edge_descriptor Edge;

      

  private:
    struct CleanPathSubstitution;
    
    /** An expression factory */
    ExprFactory &efac;
    /** An interpolating smt solver */
    ItpSmt &smt;
    
  public:

    DagInterpolateMulti (ExprFactory &f, ItpSmt &itpSmt) : 
      efac(f), smt (itpSmt) {}
    
    /** Computes a DAG interpolant.
     * 
     * \tparam Range a boost::range
     * \tparam EdgeMap a property map frome Edges to Expr
     * \tparam VertexMap a property map from Vertecies to Expr
     * 
     * \param[in] g the input graph
     * \param[in] topo topological order of vertices of g
     * \param[in] emap a property map from edges of g to Expr
     * \param[out] vmap DAG interpolant as a vertex map
     * 
     * \return true if there is an interpolant, false if the formula is
     * satisfiable, and indeterminate if SMT-solver failed to decide the
     * formula.
     */
    template <typename Range, typename EdgeMap, typename VertexMap>
    tribool dagItp (const Graph &g, Range &topo, 
		    const EdgeMap &emap, VertexMap &out)
    {
      // -- encode verification condition of the DAG
      ExprVector vc;
      encodeVC (g, topo, emap, std::back_inserter (vc));

      // -- path-interpolant is stored here
      ExprVector pitp;
      // -- compute interpolants
      tribool res = smt.interpolate (vc.begin (), vc.end (), 
				     std::back_inserter (pitp));
    
      // -- clean the path-interpolant and populate the output
      if (res) cleanPathItp (pitp, g, topo, emap, out);    
      return res;
    }
    
  private:

    /**
     * Compute VC of the DAG. 
     *
     * VC is a collection of formulas that is satisfiable iff there
     * exists a path through the graph g such that the conjunction of
     * edge labels on the path is satisfiable.
     *
     * \tparam Range a boost::range
     * \tparam EdgeMap a property map frome Edges to Expr
     * \tparam OutputIterator STL OutputIterator
     * 
     * \param[in] g the input graph
     * \param[in] topo topological order of vertices of g
     * \param[in] emap a property map from edges of g to Expr
     * \param[out] vc an output iterator to store generated VC
     * 
     */
    template <typename Range, typename EdgeMap, typename OutputIterator>
    void encodeVC (const Graph &g, Range &topo, const EdgeMap &emap, 
		   OutputIterator vc)
    {
      // -- for each vertex in topological order
      foreach (Vertex v, topo)
	{
	  // -- variable for the node
	  Expr vTerm = mkTerm (v, efac);

	  // -- partial result, one entry per each edge
	  ExprVector subRes;
	  // -- variables for the edges for the mutex constraint
	  ExprVector edgVars;

	  // -- for each edge
	  foreach (Edge uc, out_edges (v, g))
	    {
	      
	      // -- variable for the child node 'c'
	      Expr cTerm = mkTerm (target (uc, g), efac);
	      
	      Expr edgeC = get (emap, uc);
	      // -- combine resolvers if they are given
	      if (isOpX<TUPLE> (edgeC))
		edgeC = mk<AND> (edgeC->left (), edgeC->right ());

	      // -- edge condition, depends on child node	      
	      edgeC = boolop::land (cTerm, edgeC);
	      
	      // -- allocate variable for the edge (used later)
	      edgVars.push_back (mkEdgVar (vTerm, cTerm));

	      
	      // -- store current edge condition
	      subRes.push_back (edgeC);
	    }

	  // -- result is a disjunction of all edge conditions
	  Expr res = mknary<OR> (mk<FALSE> (efac), 
				 subRes.begin (),
				 subRes.end ());

	  // -- conditional on the vertex variable, unless v is the root
	  if (!isRoot (v, g))
	    {
	      res = boolop::limp (vTerm, res);

	      // -- exactly one edge from v can be active 
	      Expr mex = mk<IMPL> (vTerm, exactlyOne (edgVars, efac));
	      res = boolop::land (res, mex);
	    }

	  *(vc++) = res;
	}
    }    
    
    Expr mkEdgVar (Expr src, Expr dst)
    {
      return bind::boolVar (mk<TUPLE> (src, dst));
    }
    
    /**
     * Converts a path-interpolant into a DAG interpolant by eliminating
     * out-of-scope variables.
     * 
     * \tparam Range a boost::range
     * \tparam EdgeMap a property map frome Edges to Expr
     * \tparam VertexMap a property map from Vertecies to Expr
     * 
     * \param[in] g the input graph
     * \param[in] topo topological order of vertices of g
     * \param[in] emap a property map from edges of g to Expr
     * \param[out] vmap DAG interpolant as a vertex map
     * 
     */
    template <typename Range, typename EdgeMap, typename VertexMap>  
    void cleanPathItp (const ExprVector &itp, const Graph &g,
		       Range &topo, const EdgeMap &emap, 
		       VertexMap &vmap)
    {
      // -- for each (vertex, expression) in  (topological order, itp)
      typedef boost::tuple<const Vertex, const Expr> TUPLE;
      
      foreach (TUPLE tpl, 
	       make_pair
	       (make_zip_iterator (make_tuple (begin (topo), begin (itp))),
		make_zip_iterator (make_tuple (end (topo), end (itp)))))
	{
	  Vertex v = tpl.get<0> ();
	  Expr e = tpl.get<1> ();
	  
	  // -- root node
	  if (isRoot (v, g))
	    {
	      put (vmap, v, e);
	      continue;
	    }

	  Expr vTerm = mkTerm (v, efac);

	  ExprVector subRes;
	  foreach (Edge edg, in_edges (v, g))
	    {
	      
	      // -- the term for the source node
	      Expr uTerm = mkTerm (source (edg, g), efac);

	      // -- expression at the source
	      Expr uExpr = get (vmap, source (edg, g));

	      // -- substitute things appropriatelly
	      CleanPathSubstitution cps (uTerm, vTerm);
	      uExpr = replaceSimplify (uExpr, mk_fn_map (cps));
	      
	      uExpr = boolop::land (uExpr, get (emap, edg));
	      
	      subRes.push_back (uExpr);
	      
	    }
	  
	  ExprVector evec;
	  evec.push_back (mknary<OR> (mk<FALSE>(efac), 
				      subRes.begin (), subRes.end ()));
	  Expr vExpr = get (vmap, v);
	  vExpr = replaceAllSimplify (vExpr, vTerm, mk<TRUE> (efac));
	  evec.push_back (boolop::lneg (vExpr));
	  
	  ExprVector newLabel;
	  
	  tribool b = smt.interpolate (evec.begin (), evec.end (), 
				       std::back_inserter (newLabel));
	  // -- interpolation must succeed here
	  assert (b);
	  // -- store result in the map
	  put (vmap, v, newLabel [0]);
	}
    }	       


    /**
     * Returns true if v is a root of graph g
     * 
     * \param[in] v vertex
     * \param[in] g graph
     * 
     * \return true if v is root of g
     */
    bool isRoot (Vertex v, const Graph &g)
    {
      return in_degree (v, g) == 0;
    }

    /** Substitution used in cleanPathItp() method. */
    struct CleanPathSubstitution
    {
      typedef expr::Terminal<Vertex> VERTEX;
	
      Expr good1;
      Expr good2;
      /**
	 \param[in] g1 An expression to be replaced by TRUE
	 \param[in] g2 An expression to be replaced by TRUE
      */
      CleanPathSubstitution (Expr g1, Expr g2) : good1(g1), good2(g2) {}
	
      Expr operator () (Expr e) const
      {
	if (e == good1 || e == good2) return mk<TRUE> (e->efac ());
	if (isOpX<VERTEX> (e)) return mk<FALSE> (e->efac ());
	return Expr (0);
      }
    };
      


  };
}

  
  
		     

#endif
