#include "llvm/Analysis/Dominators.h"

#include "ufo/ufo.hpp"
#include "ufo/Expr.hpp"
#include "ufo/Cpg/Lbe.hpp"
#include "ufo/Arg.hpp"
#include "ufo/ArgBgl.hpp"
#include "ufo/ArgCond.hpp"

#include "boost/graph/topological_sort.hpp"
#include "ufo/ufo_graph.hpp"

#include "ufo/Smt/UfoZ3.hpp"

#include "ArgClausifier.hpp"

#include "boost/range/algorithm/sort.hpp"
#include "boost/range/algorithm/set_algorithm.hpp"
#include "boost/range/algorithm/transform.hpp"

using namespace ufo;
using namespace llvm;

namespace
{

  struct AsmPredicate : std::unary_function<Expr,bool>
  { bool operator() (const Expr &v) const { return isOpX<ASM> (v); } };

  struct StripAssumptions : std::unary_function<Expr,Expr>
  {
    Expr operator() (const Expr &v) const 
    {
      assert (isOpX<ASM> (v));
      return v->left ();
    }
  };
    

  /// bottom-up MUS
  template<typename SMT, typename OutputIterator>
  tribool mus_bup (SMT &smt, Expr phi, OutputIterator out)
  {
    smt.assertExpr (phi);
    
    ExprVector orig;
    filter (phi, AsmPredicate(), std::back_inserter (orig));
    
    ExprVector forcedTrue;
    forcedTrue.reserve (orig.size ());
    
    while (true)
      {
        tribool res = smt.solveAssuming (forcedTrue);
        if (!res || res == indeterminate) return res;

        // -- no more assumptions left. The formula is satisfiable
        // -- even if all assumptions are used.
        if (orig.empty ()) return true;

        typename SMT::Model m = smt.getModel ();
        ExprVector::iterator first = orig.begin ();
        ExprVector::iterator last = orig.end ();        
        ExprVector::iterator result = first;

        // force assumptions that are False in the current model to True
        while (first != last)
          {
            Expr ai = *first;
            if (!isOpX<FALSE> (m.eval (ai)))
              {
                *result = *first;
                ++result;
              }
            else
              {
                *(out++) = ai;
                forcedTrue.push_back (ai);
              }
            ++first;
          }
        orig.resize (std::distance (orig.begin (), result));        
      }
  }
  

  class ArgPropagator
  {
  private:
    ExprFactory &m_efac;

    DominatorTree &m_dt;
    LBE &m_lbe;
    
    ARG &m_arg;
    Node *m_entry;
    Node *m_exit;
    NodeExprMap &m_lemmas;

  public:
    ArgPropagator (ExprFactory &efac,
		   DominatorTree &dt,
		   LBE &lbe,
		   ARG &arg, Node* entryN, Node *exitN,
		   NodeExprMap &lemmas):
      m_efac (efac), m_dt (dt), m_lbe (lbe),
      m_arg (arg), m_entry (entryN), m_exit (exitN),
      m_lemmas (lemmas) {}
    
    typedef std::map<Node*,Environment> NodeEnvMap;
    typedef std::map<NodePair,Expr> EdgeExprMap;
    
    void run ()
    {
      NodeSet reachable;
      ReachARG rArg (ufo::mk_reachable (m_arg, m_entry, m_exit, reachable));
      NodeList topo;
      boost::topological_sort (rArg, std::front_inserter (topo));

      // -- compute arg condition
      ArgCond< ReachARG > aCondComp (m_efac, m_lbe, m_dt, false);
      NodeEnvMap envMap;
      EdgeExprMap argCond;
      std::map<NodePair,Environment> edgEnvMap;
      BOOST_AUTO (envMapPM, make_assoc_property_map (envMap));
      BOOST_AUTO (argCondPM, make_assoc_property_map (argCond));
      BOOST_AUTO (edgEnvPM, make_assoc_property_map (edgEnvMap));
      aCondComp.argCond (rArg, topo, envMapPM, argCondPM, edgEnvPM);

      
      typedef std::map<CutPointPtr, Node*> LocMap;
      LocMap locMap;
      UfoZ3 z3(m_efac);


      forall (Node *n, topo)
	{
	  Node *prev = NULL;
	  if (locMap.count (n->getLoc ()) > 0)
	    prev = locMap [n->getLoc ()];
	  locMap [n->getLoc ()] = n;

	  // nothing to propagate
	  if (prev == NULL) continue;

	  // -- skip nodes with no lemmas. There should not be any
          assert (m_lemmas.count (n) > 0);
	  if (m_lemmas.count (n) <= 0) continue;	  

	  // -- skip lemmas that cannot be strengthened
	  if (isOpX<FALSE> (m_lemmas [n])) continue;
	  
	  m_lemmas [n] = propagateNode (n, prev, rArg, argCond, envMap, z3);
	}
    }

  private:
    
    /// propagate lemmas of p to n. Assume p->getLoc () == n->getLoc ()
    Expr propagateNode (Node *n, Node *p, 
			ReachARG &arg, EdgeExprMap &argCond, NodeEnvMap &envs, 
			UfoZ3 &z3)
    {
      assert (n->getLoc () == p->getLoc ());

      Expr nLemma = m_lemmas [n];
      Expr pLemma = m_lemmas [p];

      LOG("propagator_verbose",
          dbgs () << "pLemma is " << *pLemma << "\n";);
      
      ExprVector nClauses;
      nClauses.reserve (nLemma->arity ());
      nLemma = extractClauses (nLemma, std::back_inserter (nClauses));
      if (!isOpX<TRUE> (nLemma)) nClauses.push_back (nLemma);
      boost::sort (nClauses);
      
      ExprVector pClauses;
      pClauses.reserve (pLemma->arity ());
      pLemma = extractClauses (pLemma, std::back_inserter (pClauses));
      if (!isOpX<TRUE> (pLemma)) pClauses.push_back (pLemma);
      boost::sort (pClauses);

      LOG ("propagator_verbose",
           dbgs () << "pClauses are: \n";
           forall (Expr c, pClauses) dbgs () << *c << "\n";
           dbgs () << "pClauses end\n";);
      
      ExprVector lemmas;
      lemmas.reserve (pClauses.size ());
      boost::set_difference (pClauses, nClauses, std::back_inserter (lemmas));
      LOG ("propagator_verbose",
           dbgs () << "Lemmas are: \n";
           forall (Expr l, lemmas) dbgs () << *l << "\n";
           dbgs () << "Lemmas end\n";);

      // -- no lemmas to propagate. Only possible if the loop has
      // -- converged.
      if (lemmas.empty ()) return m_lemmas [n];
      

      // -- build up SMT context to check if the lemmas are true at n
      ExprVector inEdges;
      inEdges.reserve (boost::in_degree (n, arg));
      forall (NodePair un, boost::in_edges (n, arg))
	{
	  // -- skip false edges
	  if (boost::get (false_edge_prop_t(), arg, un)) continue;

	  Node *u = boost::source (un, arg);
	  Expr edg = argCond [un];
	  if (isOpX<TUPLE> (edg))
	    edg = mknary<AND> (edg->args_begin (), edg->args_end ());
	  edg = boolop::land (envs [u].eval (m_lemmas [u]), edg);
	  inEdges.push_back (edg);
	}
      
      ZSolver<UfoZ3> smt (z3);
      smt.assertExpr (mknary<OR> (mk<TRUE> (m_efac), inEdges));
      forall (Expr e, nClauses) smt.assertExpr (envs [n].eval (e));

      // -- construct a formula !a1&&!l1 || !a2&&!l2 || !a3&&!l3,
      // -- where {a_i} are assumptions and {l_i} are lemmas. An a_i
      // -- is in an unsat core if l_i is not valid at n. Compute MUS,
      // -- and remove lemmas that appear in MUS.
      Expr phi = mk<FALSE> (m_efac);
      forall (Expr l, lemmas)
        {
          // -- create an assumption. The name is the original lemma
          Expr lasm = mk<ASM> (l);
          // -- evaluate lemma in the context of n
          Expr lem = envs [n].eval (l);
          phi = boolop::lor (phi, mk<AND> (mk<NEG> (lasm), 
                                           boolop::lneg (lem)));
        }
      LOG ("propagator_verbose",
           dbgs () << "phi is " << *phi << "\n";);
      
      ExprVector badLemmas;
      badLemmas.reserve (lemmas.size ());
      tribool smt_res = mus_bup (smt, phi, std::back_inserter (badLemmas));
      UFO_ASSERT (!smt_res);

      // -- all lemmas are bad, nothing was propagated
      if (badLemmas.size () == lemmas.size ()) return m_lemmas [n];
      
      // -- strip assumptions from bad lemmas
      boost::transform (badLemmas, badLemmas.begin (), StripAssumptions());

      // -- find all good lemmas
      boost::sort (badLemmas);
      ExprVector goodLemmas;
      goodLemmas.reserve (lemmas.size ());
      boost::set_difference (lemmas, badLemmas, 
                             std::back_inserter (goodLemmas));

      LOG("propagator",
          dbgs () << "Propagated " 
          << goodLemmas.size () << " / " << lemmas.size () 
          << " lemmas to " << n->getId () << " from " << p->getId () << "\n";);
      
      
      LOG("propagator_verbose",
          dbgs () << "Propagated lemmas are:\n";
          forall (Expr l, goodLemmas) dbgs () << "\t" << *l << "\n";
          dbgs () << "Propagated lemmas end\n";);
      
      // add to nClauses and return
      boost::copy (goodLemmas, std::back_inserter (nClauses));
      
      return mknary<AND> (mk<TRUE> (m_efac), nClauses);
    }
  };
    
}

namespace ufo
{
  /// propagate lemmas by pushing them forward
  void propagateArgLemmas (ExprFactory &efac, DominatorTree &dt, LBE &lbe, 
			   ARG &arg, Node *entryN, Node *exitN,
			   NodeExprMap &lemmas);
}



void ufo::propagateArgLemmas (ExprFactory &efac, DominatorTree &dt, LBE &lbe, 
			      ARG &arg, Node *entryN, Node *exitN,
			      NodeExprMap &lemmas)
{
  ArgPropagator ap (efac, dt, lbe, arg, entryN, exitN, lemmas);
  ap.run ();
}
