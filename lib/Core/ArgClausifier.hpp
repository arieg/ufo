#ifndef _ARG_CLAUSIFIER_HPP_
#define _ARG_CLAUSIFIER_HPP_

#include "ufo/ufo.hpp"
#include "ufo/Cpg/Lbe.hpp"
#include "ufo/Arg.hpp"
#include "boost/algorithm/cxx11/all_of.hpp"

namespace ufo
{
  void clausifyArgLemmas (ExprFactory &efac, Function &mainF,
			  DominatorTree &dt, LBE &lbe, 
			  ARG &arg, Node *entryN, Node *exitN,
			  NodeExprMap &lemmas);


  /// Clause extraction logic

  struct IsLit : std::unary_function<Expr,bool>
  {
    inline bool operator() (Expr e)
    {
      if (e->arity () == 0) return true;
      if (isOpX<NEG> (e)) return (*this) (e->left ());
      
      // treat small formulas as literals
      if (isOpX<AND> (e) || isOpX<OR> (e)) return boolop::circSize (e) <= 6;
      
      // -- everything else is a literal
      return true;
    }
  };
    
  inline bool isClause (Expr e)
  {
    IsLit isLit;
    if (isLit (e)) return true;
    if (isOpX<OR> (e))
      return boost::algorithm::all_of (e->args_begin (),
				       e->args_end (), isLit);
    return false;
  }
  
  /// extracts clauses from e and returns the remainder
  template <typename OutputIterator>
  Expr extractClauses (Expr e, OutputIterator clauses)
  {
    if (isOpX<AND> (e))
      {
        ExprVector rem;
        rem.reserve (e->arity ());
    
        forall (const Expr &a, make_pair (e->args_begin (), e->args_end ()))
          if (isClause (a)) *(clauses++) = a;
          else rem.push_back (a);
        // -- return TRUE if there is no remainder, and conjunction of
        // -- remainder otherwise
        return mknary<AND> (mk<TRUE> (e->efac ()), rem);
      }
    
    if (isClause (e))
      {
	*(clauses++) = e;
	return mk<TRUE> (e->efac ());
      }
    return e;
  }  
}




#endif /* _ARGCLAUSIFIER_H_ */
