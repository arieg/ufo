#include "ArgClausifier.hpp"
#include "llvm/Analysis/Dominators.h"

#include "ufo/ufo.hpp"
#include "ufo/Expr.hpp"
#include "ufo/Cpg/Lbe.hpp"
#include "ufo/Arg.hpp"
#include "ufo/ArgBgl.hpp"

#include "ufo/ExprAig.hpp"

#include "boost/graph/topological_sort.hpp"
#include "ufo/ufo_graph.hpp"

#include "ufo/Smt/UfoZ3.hpp"
#include "ufo/CptLiveValues.hpp"


#include "boost/range/algorithm/copy.hpp"
#include "boost/range/algorithm/sort.hpp"



// -- for tmpnam
#include <cstdio>

using namespace ufo;
using namespace llvm;

namespace
{
  class ArgClausifier
  {
  private:
    ExprFactory &m_efac;

    Function &m_mainF;
    DominatorTree &m_dt;
    LBE &m_lbe;
    
    ARG &m_arg;
    Node *m_entry;
    Node *m_exit;
    NodeExprMap &m_lemmas;

    CptLiveValues m_cpLive;

    class LinearRule;
    typedef std::pair<CutPointPtr,CutPointPtr> CutPointPair;
    typedef std::map<CutPointPair,LinearRule> RuleMap;
    RuleMap m_rules;
    
    
  public:
    ArgClausifier (ExprFactory &efac,
		   Function &mainF,
		   DominatorTree &dt,
		   LBE &lbe,
		   ARG &arg, Node* entryN, Node *exitN,
		   NodeExprMap &lemmas):
      m_efac (efac),
      m_mainF (mainF), m_dt (dt), m_lbe (lbe),
      m_arg (arg), m_entry (entryN), m_exit (exitN),
      m_lemmas (lemmas),
      m_cpLive (m_efac, 
                m_mainF, m_entry->getLoc (), m_exit->getLoc (), 
                m_dt, m_lbe) 
    { 
      m_cpLive.run ();
      createRules (); 
    }
    
    void run ()
    {
      NodeSet reachable;
      ReachARG rArg (ufo::mk_reachable (m_arg, m_entry, m_exit, reachable));
      NodeList topo;
      boost::topological_sort (rArg, std::front_inserter (topo));

      
      typedef std::map<CutPointPtr, Node*> LocMap;
      
      LocMap locMap;

      UfoZ3 z3(m_efac);


      forall (Node *n, topo)
	{

	  // -- skip nodes with no lemmas
	  if (m_lemmas.count (n) <= 0) continue;	  

	  Expr nLemma = m_lemmas [n];

	  // -- skip constants
	  if (isOpX<TRUE> (nLemma) || isOpX<FALSE> (nLemma)) continue;
	  // -- skip lemmas that do not have any Boolean structure
	  if (!isOp<BoolOp> (nLemma)) continue;
	  
	  m_lemmas [n] = clausifyLemma (n, rArg, z3);
	}
    }

  private:      
    
    /// Linear Horn rule.
    class LinearRule
    {
      /// interpreted tail
      Expr m_itail;
      CutPointPtr m_src;
      CutPointPtr m_dst;

      /// environments for source and destination of the rule. 
      // XXX Probably do not want them here. Need to think about this.
      Environment m_srcEnv;
      Environment m_dstEnv;

      /// signature of the src
      ExprVector m_srcArgs;
      /// signature of the dst
      ExprVector m_dstArgs;

      /// all variables used by the rule
      ExprVector m_vars;
      
    public:
      void setSrc (CutPointPtr cp) { m_src = cp; }
      void setDst (CutPointPtr cp) { m_dst = cp; }

      void setSrcEnv (Environment &v) { m_srcEnv = v; }
      Environment &getSrcEnv () { return m_srcEnv; }

      void setDstEnv (Environment &v) { m_dstEnv = v; }
      Environment &getDstEnv () { return m_dstEnv; }
	
      const ExprVector &getDstArgs () const { return m_dstArgs; }
      const ExprVector &getSrcArgs () const { return m_srcArgs; }

      const ExprVector &getVars () 
      { 
	if (m_vars.empty ())
	  {
	    ExprVector all;
	    all.reserve (1 + m_srcArgs.size () + m_dstArgs.size ());

	    all.push_back (m_itail);
	    boost::copy (m_srcArgs, std::back_inserter (all));
	    boost::copy (m_dstArgs, std::back_inserter (all));

	    filter (mknary<TUPLE> (all), ufo::IsVar (), 
		    std::back_inserter (m_vars));
	  }
	
	return m_vars; 
      }

      Expr getITail () const { return m_itail; }      
      void setITail (Expr v) { m_itail = v; }

      void addSrcArg (Expr e) { m_srcArgs.push_back (e); }
      void addDstArg (Expr e) { m_dstArgs.push_back (e); }

    };

    void createRules ()
    {
      forall (CutPointPtr cp, m_lbe.getFunctionCutPts (m_mainF))
	{
	  for (edgeIterator it = cp->predBegin (), end = cp->predEnd (); 
	       it != end; ++it)
	    {
	      SEdgePtr edg = *it;
	      CutPointPtr src = edg->getSrc ();
	      CutPointPtr dst = edg->getDst ();
	      LinearRule r (createRule (edg));
              // store the rule
              m_rules.insert (RuleMap::value_type (std::make_pair (src, dst), r));
	    }
	}
    }
    
    LinearRule createRule (SEdgePtr edg)
    {
      LinearRule r;

      CutPointPtr src = edg->getSrc ();
      CutPointPtr dst = edg->getDst ();
      
      r.setSrc (src);
      r.setDst (dst);
	      
      Environment env ((m_efac));
	      
      // include the phi node declarations
      SEdgeCondComp::updateEnvToFirstInst (env, *src);

      // eval all src args
      forall (Expr var, m_cpLive.live (src)) r.addSrcArg (env.eval (var));
      r.setSrcEnv (env);
	      
      // compute edge between src and dst
      Expr tail = (SEdgeCondComp::computeEdgeCond (m_efac, env, edg, m_lbe));

      r.setITail (tail);

      // eval all dst args
      forall (Expr var, m_cpLive.live (dst)) r.addDstArg (env.eval (var));
      r.setDstEnv (env);
      return r;
    }
    

    LinearRule &getRule (Node* src, Node* dst)
    { return m_rules [std::make_pair (src->getLoc (), dst->getLoc ())]; }
    

    /// clausify lemma at n
    Expr clausifyLemma (Node *n, ReachARG &arg, UfoZ3 &z3)
    {
      Expr nLemma = m_lemmas [n];
      nLemma = boolop::nnf (boolop::flat_aig (nLemma));

      ExprVector clauses;
      nLemma = extractClauses (nLemma, std::back_inserter (clauses));
      
      assert (!isOpX<FALSE> (nLemma));
      
      // -- the lemma is already in clausal form
      if (isOpX<TRUE> (nLemma))
	return  mknary<AND> (mk<FALSE> (m_efac), clauses);

      typedef graph_traits<ReachARG>::edge_descriptor Edge;

      ZFixedPoint<UfoZ3> fp (z3);
      ZParams<UfoZ3> params (z3);
      params.set (":engine", "pdr");
      params.set (":use-farkas", true);
      params.set (":generate-proof-trace", false);
      params.set (":slice", false);
      params.set (":inline-linear", false);
      params.set (":inline-eager", false);
      fp.set (params);

      Expr name = mkTerm (std::string (n->getLoc ()->getBB ()->getName ()), 
			  m_efac);
      ExprVector types;
      forall (Expr var, m_cpLive.live (n->getLoc ())) 
	types.push_back (ufo::typeOf (var));
      types.push_back (mk<BOOL_TY> (m_efac));
      Expr nDecl = bind::fdecl (name, types);
      fp.registerRelation (nDecl);

      forall (Edge edg, boost::in_edges (n, arg))
	{
	  // -- skip false edges
	  if (boost::get (false_edge_prop_t(), arg, edg)) continue;
	  
	  Node *src = boost::source (edg, arg);
	  LinearRule &rule = getRule (src, n);

	  Expr nPred = bind::fapp (nDecl, rule.getDstArgs ());
	  Expr sLemma = rule.getSrcEnv ().eval (m_lemmas [src]);
	  
	  Expr body = boolop::limp (mk<AND> (rule.getITail (), sLemma), nPred);
	  fp.addRule (rule.getVars (), body);
	} 
      
      

      Expr nApp = bind::fapp (nDecl, m_cpLive.live (n->getLoc ()));

      // -- dummy exit relation
      Expr eDecl = bind::boolConstDecl (mkTerm (std::string ("!UFO!"), m_efac));
      fp.registerRelation (eDecl);
      Expr eApp = bind::fapp (eDecl);

      fp.addRule (m_cpLive.live (n->getLoc ()), 
		  mk<IMPL> (boolop::land (nApp, boolop::lneg (nLemma)), eApp));


      if (!clauses.empty ())
	fp.addCover (nApp, mknary<AND> (mk<FALSE> (m_efac), clauses));

      Expr query = eApp;
      LOG ("clausify_verbose",
	   dbgs () << "Solving FP: \n" << fp.toString (query) << "\n";);      

      
      Stats::resume ("clausifier.query");      
      Stopwatch sw;
      tribool qres = fp.query (query);
      sw.stop ();
      if (sw.toSeconds () >= 2.0)
	{
	  std::string errInfo;
	  std::string fname  (tmpnam (NULL));
	  fname = fname + "_" + lexical_cast<std::string>(sw.toSeconds ()) + ".smt2";
	  errs () << "FP QUERY file: " << fname << "\n";
	  raw_fd_ostream out (fname.c_str (), errInfo);
	  out << fp << "\n";
	  out.flush ();
	  assert (!out.has_error ());
	  out.close ();

          LOG ("clausify_verbose",
               dbgs () << "Clause part: "
               << *(mknary<AND> (mk<FALSE> (m_efac), clauses)) << "\n";
               dbgs () << "nLemma part: " << *nLemma << "\n";);
	}
      errs () << "FP QUERY: " << sw <<"\n";
      Stats::stop ("clausifier.query");      

      if (! static_cast<bool>(!qres) )
	{
	  errs () << "FAILED FP: \n" << fp << "\n";

	  std::string errInfo;
	  raw_fd_ostream out ("/tmp/ufo_failed.smt2", errInfo);
	  out << fp << "\n";
	  out.flush ();
	  if (!clauses.empty ())
	    out << "; extra cover\n"
		<< z3.toSmtLib (mknary<AND> (mk<FALSE> (m_efac), clauses))
		<< "\n";

	  out.close ();
	  assert (0 && "FAILED FP\n");
	}
      
      UFO_ASSERT (!qres && "UNEXPECTED");

      Expr res = boolop::gather (boolop::nnf (fp.getCoverDelta (nApp)));

      LOG ("clausify_verbose", 
	   dbgs () << "Converted a lemma from: " << *nLemma 
	   << " to " << *(res) << "\n";);

      LOG("clausify",
	  UFO_ASSERT (!z3n_is_sat (mk<AND> (res, mk<NEG> (nLemma)))););
      return res;
    }
  };
}



void ufo::clausifyArgLemmas (ExprFactory &efac, 
			     Function &mainF,
			     DominatorTree &dt,
			     LBE &lbe, 
			     ARG &arg,
			     Node *entryN,
			     Node *exitN,
			     NodeExprMap &lemmas)
{
  ArgClausifier ac (efac, mainF, dt, lbe, arg, entryN, exitN, lemmas);
  ac.run ();
}
