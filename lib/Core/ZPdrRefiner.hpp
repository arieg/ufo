#ifndef __ZPDR_REFINER__HPP__
#define __ZPDR_REFINER__HPP__

/** 
 * Refiner based on GPDR algorithm of Z3.
 * 
 * XXX Started as a copy-paste of DagItpRefiner.hpp. Need to merge the
 * two.
 */

#include "Refiner.hpp"

#include "boost/scoped_ptr.hpp"
#include "ufo/CptLiveValues.hpp"  
namespace ufo
{
  class ZPdrRefiner : public Refiner
  {
  private:

    boost::scoped_ptr<CptLiveValues> m_live;
    
  public:
    typedef std::map<const BasicBlock*, Expr>  BbExprMap;

    ZPdrRefiner (ExprFactory &fac, 
		   AbstractStateDomain &dom,
		   LBE &cpG,
		   DominatorTree &dt,
		   ExprMSat &msat,
		   ARG &a, bool doSimp, NodeExprMap& nodeLabels) :
      Refiner (fac, dom, cpG, dt, msat, a, doSimp, nodeLabels), 
      m_live (0) {}

    void refine ();

  };    
}




#endif
