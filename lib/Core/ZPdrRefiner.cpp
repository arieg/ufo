#include "ZPdrRefiner.hpp"

#include "ufo/Arg.hpp"
#include "ufo/ArgBgl.hpp"
#include "ufo/ArgCond.hpp"

#include "boost/graph/topological_sort.hpp"
#include "ufo/property_map.hpp"
#include "ufo/ufo_graph.hpp"


#include "ZPdrDagInterpolator.hpp"

#include "ufo/Smt/MUS.hpp"
#include "ufo/property_map.hpp"
#include "ufo/ufo_iterators.hpp"

// -- for addAssumptions and KillAssumptions 
#include "exprVisit.hpp"  

    
void ufo::ZPdrRefiner::refine ()
{
  assert (entryN != NULL);
  assert (exitN != NULL);
      
  // -- compute live values
  m_live.reset (new CptLiveValues (efac, *(arg.getFunction ()), 
				   entryN->getLoc (),
				   exitN->getLoc (),
				   DT, lbe));
  m_live->run ();
      

  /** Counterexamples are only computed once. There is no
   * refinement after that 
   */
  assert (argCex.empty ());
  assert (cfgCex.empty ());
      

  labels.clear ();      
  labels [entryN] = mk<TRUE> (efac);
  labels [exitN] = mk<FALSE> (efac);
      
  // -- compute the set of nodes that are between entryN and exitN
  NodeSet reachable;
  ReachARG rArg (ufo::mk_reachable (arg, entryN, exitN, reachable));
      
  // -- compute topological order of the ARG
  NodeList topo;
  topological_sort (rArg, std::front_inserter (topo));

  // errs () << "topo-sort\n";
  // foreach (Node *n, topo) errs () << "NID(" << n->getId () << ")\n";

  // -- compute the arg condition (with assumptions)
  ArgCond< BOOST_TYPEOF(rArg) > aCondComp (efac, lbe, DT, true);
  std::map<Node*,Environment> envMap;
  std::map<NodePair,Expr> argCond;
  std::map<NodePair,Environment> edgEnvMap;
  BOOST_AUTO (envMapPM, make_assoc_property_map (envMap));
  BOOST_AUTO (argCondPM, make_assoc_property_map (argCond));
  BOOST_AUTO (edgEnvPM, make_assoc_property_map (edgEnvMap));
  aCondComp.argCond (rArg, topo, envMapPM, argCondPM, edgEnvPM);

  NodeExprMap labelMap;

  {
    ExprVector vc (distance (topo));
    encodeVC (efac, rArg, topo, argCondPM, vc.begin ());


    if (consRefine)
      {
	// -- add lemmas from nodeLabels
	typedef boost::tuple<Node*, Expr&> VE;
	foreach (VE ve, 
		 make_pair
		 (mk_zip_it (++boost::begin (topo), ++boost::begin (vc)),
		  mk_zip_it (--boost::end (topo), --boost::end (vc))))
	  {
	    Node *v = ve.get<0> ();
	    Expr vTerm = mkTerm (v, efac);
	    
	    // -- evaluate the label in the environment
	    Expr nodeLabel = envMap [v].eval (nodeLabels [v]);
	    // -- add assumptions
	    addAssumptions addV;
	    nodeLabel = dagVisit (addV, boolop::nnf (nodeLabel));
	    // -- add assumption literal for the whole label
	    nodeLabel = boolop::limp (mk<ASM> (vTerm), nodeLabel);
	    
	    // -- store the label 
	    labelMap[v] =  nodeLabel;

	    // -- add the label to the VC
	    Expr &e = ve.get<1> ();
	    e = boolop::land (boolop::limp (vTerm, nodeLabel), e);

	  }
      }	

    // -- compute MUS
    ExprSet usedAssumes;
    UfoZ3 z3Ctx (efac);
    ZSolver<UfoZ3> z3 (z3Ctx);

    Stats::resume ("refiner.mus");
    tribool res = mus_basic (z3, 
			     mknary<AND> (mk<TRUE> (efac), 
					  vc.begin (), vc.end ()),
			     std::inserter (usedAssumes, 
					    usedAssumes.begin ()), 
			     3);
    Stats::stop ("refiner.mus");
	
    // -- SAT
    if (res || res == indeterminate)
      {
	labels.clear ();
	// XXX Produce CEX here
	return;
      }

    Stats::resume ("refiner.kill.assumptions");
    // -- simplify ArgCond by keeping only used assumptions
    typedef std::map<NodePair,Expr>::value_type KV;
    foreach (KV kv, argCond) 
      {
	KillAssumptions ka (usedAssumes);
	argCond [kv.first] = replaceSimplify (kv.second, mk_fn_map (ka));
      }

    if (consRefine)
      {
	typedef NodeExprMap::value_type VT;
	foreach (VT kv, labelMap)
	  {
	    KillAssumptions ka (usedAssumes);
	    labelMap [kv.first] = replaceSimplify (kv.second, 
						   mk_fn_map (ka));
	  }
      }
    Stats::stop ("refiner.kill.assumptions");
  }
      
      
      
			 
  Stats::resume ("refiner.zpdr.total");
  Stats::resume ("refiner.zpdr");

      
  // // -- compute DAG Interpolant
  ZPdrDagInterpolator< BOOST_TYPEOF(rArg)> dagItpComp (efac, *m_live);
      
  BOOST_AUTO(labelsPM, make_assoc_property_map (labels));
      
  tribool res;
  if (consRefine)
    {
      BOOST_AUTO (labelMapPM, make_assoc_property_map (labelMap));
      res = dagItpComp.dagItp (rArg, topo, envMapPM, 
			       argCondPM, labelMapPM, labelsPM);
    }
  else
    {
      static_property_map<Expr> smap(mk<TRUE> (efac));
      res = dagItpComp.dagItp (rArg, topo, envMapPM, 
			       argCondPM, smap, labelsPM);
    }      
	
  Stats::stop ("refiner.zpdr");

  if (res)
    {
      // typedef std::pair<Node*,Expr> KV;
      // errs () << "New labels\n";
      // foreach (KV kv, labels)
      //   errs () << kv.first->getId () << " " << *kv.second << "\n";
	  
      assert (labels.size () == topo.size ());
	  

      Stats::resume ("refiner.nnf");	  
      // -- put labels into NNF (required by other passes)
      for (NodeExprMap::iterator it = labels.begin (), end = labels.end ();
	   it != end; ++it)
	it->second = boolop::gather (boolop::nnf (it->second));
      Stats::stop ("refiner.nnf");	  	  

      if (consRefine)
	{
	  typedef NodeExprMap::value_type KV;
	  foreach (KV kv, labels)
	    {
	      if (isLeaf (kv.first, rArg) || 
		  isRoot (kv.first, rArg)) continue;
		  
	      NodeExprMap::const_iterator it = nodeLabels.find (kv.first);
	      assert (it != nodeLabels.end ());
		  
	      labels [kv.first] = boolop::land (kv.second, it->second);
	    }
	}  

      assert (isOpX<TRUE> (labels [entryN]));
      assert (isOpX<FALSE> (labels [exitN]));
    }
  else labels.clear ();

  Stats::stop ("refiner.zpdr.total");
}
