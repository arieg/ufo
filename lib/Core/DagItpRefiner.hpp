#ifndef __DAG_ITP_REFINER__HPP__
#define __DAG_ITP_REFINER__HPP__

/** A refiner based on DagInterpolate and ArgCond classes. 
 * 
 * Should replace the standard Refiner eventually.
 */

#include "ufo/ufo.hpp"

#include "ufo/Cpg/Lbe.hpp"
#include "ufo/Arg.hpp"
#include "ufo/ArgBgl.hpp"
#include "ufo/ArgCond.hpp"

#include "ufo/EdgeComp.hpp"

#include "Refiner.hpp"
#include "ufo/Smt/ExprMSat.hpp"
#include "ufo/Smt/UfoZ3.hpp"

#include "boost/range/adaptor/map.hpp"
#include "ufo/ufo_iterators.hpp"

#include "UfoLinerPrinter.hpp"

namespace ufo
{
  class DagItpRefiner : public Refiner
  {
  public:
    typedef std::map<const BasicBlock*, Expr>  BbExprMap;

    DagItpRefiner (ExprFactory &fac, 
		   AbstractStateDomain &dom,
		   LBE &cpG,
		   DominatorTree &dt,
		   ExprMSat &msat,
		   ARG &a, bool doSimp, NodeExprMap& nodeLabels) :
      Refiner (fac, dom, cpG, dt, msat, a, doSimp, nodeLabels) {}
    
    void refine ();
  private:
    template <typename Graph>
    void computeCex (const Graph &rArg, ZModel<UfoZ3> &m,
		     std::map<NodePair, Environment> &edgMap)
    {
      /** compute the ARG cex */
      doArgCex (rArg, entryN, exitN, m, std::back_inserter (argCex));
      
      typedef boost::tuple<Node*,Node*> NT;
      forall (NT nt, 
	      make_pair 
	      (mk_zip_it (boost::begin (argCex), ++boost::begin (argCex)),
	       mk_zip_it (--boost::end (argCex), boost::end (argCex))))
	{
	  Node* src = nt.get<0> ();
	  Node* dst = nt.get<1> ();
	  NodePair edge = std::make_pair (src, dst);
	  doCfgCex (src->getLoc ()->getBB (),
		    dst->getLoc ()->getBB (),
		    edgMap [edge], m, 
		    std::back_inserter (cfgCex));
	}
      cfgCex.push_back (argCex.back ()->getLoc ()->getBB ()); 
    } 

    void printDbgCex (raw_ostream &out, 
		      std::map<NodePair, Environment> &edgEnvMap, 
		      ZModel<UfoZ3> &model)
    {
      NodeList::const_iterator argIt = argCex.begin ();
      NodeList::const_iterator argEnd = argCex.end ();
      BbList::const_iterator cfgIt = cfgCex.begin ();
      //BbList::const_iterator cfgEnd = cfgCex.end ();

      do
	{
	  assert (argIt != argEnd);
	  assert (std::distance (argIt, argEnd) >= 2);
			
	  Node* src = *argIt;
	  ++argIt;
	  Node* dst = *argIt;
			
	  NodePair edg = std::make_pair (src, dst);
	  out << "arg edge: " 
	      << edg.first->getId () 
	      << " --> "
	      << edg.second->getId () << "\n";
			
	  assert (edgEnvMap.count (edg) > 0);
	  Environment &edgEnv = edgEnvMap [edg];
	  printLines (out, *cfgIt, model, edgEnv);
			
	  const Node *n = dst;
	  do
	    {
	      ++cfgIt;
	      const BasicBlock *bb = *cfgIt;
	      if (bb != n->getLoc ()->getBB ())
		printLines (out, bb, model, edgEnv);    
	    }
			
	  while (*cfgIt != n->getLoc ()->getBB ());
	}
      while (std::distance (argIt, argEnd) >= 2);
    } 

    template<typename Graph, typename Range, 
	     typename ArgCond, typename Envs, 
	     typename LemmasIn, typename LemmasOut>
    void strengthen (const Graph &arg, const Range &topo, 
		     ArgCond argCond, Envs envs,
		     LemmasIn lemmas, LemmasOut &out)
    {
      typedef std::map<CutPointPtr, Node*> LocMap;
      typedef graph_traits<Graph> Traits;
      typedef typename Traits::edge_descriptor Edge;
      
      LocMap locMap;
      
      forall (Node *n, topo)
	{
	  Node *prev = NULL;
	  if (locMap.count (n->getLoc ()) > 0)
	    prev = locMap [n->getLoc ()];
	  locMap [n->getLoc ()] = n;
	    
	  if (prev == NULL) continue;
	  
	  
	  UfoZ3 z3 (efac);
	  ZSolver<UfoZ3> smt (z3);
	      
	  ExprVector inEdges;
	  inEdges.reserve (boost::in_degree (n, arg));
	  
	  forall (Edge un, boost::in_edges (n, arg))
	    {
	      Node *u = boost::source (un, arg);
	      Expr edg = boost::get (argCond, un);
	      if (isOpX<TUPLE> (edg))
		edg = mknary<AND> (edg->args_begin (), edg->args_end ());
	      edg = boolop::land (boost::get (lemmas, u), edg);
	      edg = boolop::land (boost::get (out, u), edg);

	      inEdges.push_back (edg);		  
	    }
	  smt.assertExpr (mknary<OR> (mk<TRUE> (efac), inEdges));
	  smt.assertExpr (boost::get (lemmas, n));
	  Expr nLemma = boost::get (out, n);
	  smt.assertExpr (nLemma);

	  Expr fLemma = boost::get (out, prev);
	  
	  StripVariants sv;
	  if (!isOpX<TRUE> (fLemma))
	    {
	      fLemma = replace (fLemma, mk_fn_map (sv));
	      fLemma = boost::get (envs, n).eval (fLemma);
	      if (fLemma == nLemma) fLemma = mk<TRUE> (efac);
	    }

	  Expr pLemma = boost::get (lemmas, prev);
	  
	  if (!isOpX<TRUE> (pLemma))
	    {
	      pLemma = replace (pLemma, mk_fn_map (sv));
	      pLemma = boost::get (envs, n).eval (pLemma);
	      if (pLemma == nLemma) pLemma = mk<TRUE> (efac);
	    }
	  
	  fLemma = boolop::land (fLemma, pLemma);
	  if (isOpX<TRUE> (fLemma)) continue;
	  
	  smt.assertExpr (mk<NEG> (fLemma));
	      
	  if (!smt.solve ())
	    {
	      errs () << "Forced covered at: " << n->getId () << " by " 
		      << prev->getId () << "\n";
	      LOG("ditp_verbose",
		  dbgs () << "Old label: " << *nLemma << "\n"
		  << "New label: +" <<  *fLemma << "\n";);
	      
	      boost::put (out, n, boolop::land (nLemma, fLemma));
	    } 
	}
    }
  };    
}




#endif
