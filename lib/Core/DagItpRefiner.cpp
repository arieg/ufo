#include "DagItpRefiner.hpp"

#include "boost/graph/filtered_graph.hpp"
#include "boost/graph/topological_sort.hpp"
#include "ufo/ufo_graph.hpp"

#include "ufo/Smt/MUS.hpp"
#include "ufo/property_map.hpp"

#include "ufo/Smt/Abc.hpp"

#include "DagInterpolate.hpp"

// -- for addAssumptions and KillAssumptions 
#include "exprVisit.hpp"  


void ufo::DagItpRefiner::refine ()
{
  assert (entryN != NULL);
  assert (exitN != NULL);

  /** Counterexamples are only computed once. There is no
   * refinement after that 
   */
  assert (argCex.empty ());
  assert (cfgCex.empty ());
      

  labels.clear ();      
  labels [entryN] = mk<TRUE> (efac);
  labels [exitN] = mk<FALSE> (efac);
      
      
  // -- compute the set of nodes that are between entryN and exitN
  NodeSet reachable;
  ReachARG rArg (ufo::mk_reachable (arg, entryN, exitN, reachable));
      
  // -- compute topological order of the ARG
  NodeList topo;
  boost::topological_sort (rArg, std::front_inserter (topo));

  // errs () << "topo-sort\n";
  // foreach (Node *n, topo) errs () << "NID(" << n->getId () << ")\n";

  // -- compute the arg condition (with assumptions)
  ArgCond< ReachARG > aCondComp (efac, lbe, DT, true);
  std::map<Node*,Environment> envMap;
  std::map<NodePair,Expr> argCond;
  std::map<NodePair,Environment> edgEnvMap;
  BOOST_AUTO (envMapPM, make_assoc_property_map (envMap));
  BOOST_AUTO (argCondPM, make_assoc_property_map (argCond));
  BOOST_AUTO (edgEnvPM, make_assoc_property_map (edgEnvMap));
  aCondComp.argCond (rArg, topo, envMapPM, argCondPM, edgEnvPM);

  NodeExprMap labelMap;
  BOOST_AUTO (labelMapPM, make_assoc_property_map (labelMap));
      
  if (consRefine)
    {
      // -- compute lemmas from nodeLabels
      forall (NodeExprMap::value_type &vlemma, nodeLabels)
	{
	  // XXX skip null's. They really should not be there, but
	  // XXX seems that the error location is labeled by null.
	  if (!vlemma.second) continue;
	      
	  Node *v = vlemma.first;
      // XXX skip nodes that did not participate in the arg
      if (envMap.count (v) <= 0) continue;

	  Expr vTerm = mkTerm (v, efac);
          
	  // -- evaluate the label in the environment
	  Expr lemma = vlemma.second;
	  Expr nodeLemma = envMap [v].eval (lemma);

	  // -- add assumptions
	  addAssumptions addV;
	  nodeLemma = dagVisit (addV, boolop::nnf (nodeLemma));

	  // -- add assumption literal for the whole label
	  nodeLemma = boolop::limp (mk<ASM> (vTerm), nodeLemma);
	    
	  // -- store the label 
	  labelMap[v] =  nodeLemma;
	}
    }	



  {
    ExprVector vc (distance (topo));
    encodeVC (efac, rArg, topo, argCondPM, labelMapPM, vc.begin ());



    // -- compute MUS
    ExprSet usedAssumes;
    UfoZ3 z3Ctx (efac);
    ZSolver<UfoZ3> z3 (z3Ctx);
    Stats::resume ("refiner.mus");
    tribool res = mus_basic (z3, 
			     mknary<AND> (mk<TRUE> (efac), 
					  vc.begin (), vc.end ()),
			     std::inserter (usedAssumes, 
					    usedAssumes.begin ()), 
			     3);
    Stats::stop ("refiner.mus");
	
    // -- SAT
    if (res || res == indeterminate)
      {
	labels.clear ();
	if (res) 
	  { 
	    ZModel<UfoZ3> model (z3.getModel ());
		
	    if (!(ufocl::TRACE_FILENAME).empty ())
	      { 
		// -- stream to write cex to
		raw_ostream *out = &errs ();

		// -- attept to open trace file
                std::string errorStr;
		raw_fd_ostream fs (ufocl::TRACE_FILENAME.c_str (),
				   errorStr, 0);
		if (!errorStr.empty ())
		  errs () << "Cannot open: " << errorStr << "\n";
		else
		  out = &fs;
			
		errs () << "Computing CEX\n";
		Stats::resume ("refiner.cex");
		computeCex (rArg, model, edgEnvMap);
		Stats::stop ("refiner.cex");
		errs () << "Done with CEX\n";
		printDbgCex (*out, edgEnvMap, model);
		out->flush ();
	      }
	  }
	return;
      }

    Stats::resume ("refiner.kill.assumptions");
    // -- simplify ArgCond by keeping only used assumptions
    foreach (Expr &v, argCond | boost::adaptors::map_values) 
      {
	KillAssumptions ka (usedAssumes);
	v = replaceSimplify (v, mk_fn_map (ka));
      }

    if (consRefine)
      {
	foreach (Expr &v, labelMap | boost::adaptors::map_values)
	  {
            if (v)
              {
                KillAssumptions ka (usedAssumes);
                v = replaceSimplify (v, mk_fn_map (ka));
              }
	  }
      }
    Stats::stop ("refiner.kill.assumptions");
  }
      
      
      
			 
  Stats::resume ("refiner.dag.interpolate.total");
  Stats::resume ("refiner.dag.interpolate");
  // -- compute DAG Interpolant
  ExprMSat msat(efac);
  msat.restart (true);
  msat.reset (true);
  DagInterpolate< BOOST_TYPEOF(rArg), ExprMSat > dagItpComp (efac, msat);
  BOOST_AUTO(labelsPM, make_assoc_property_map (labels));
	  
  tribool res;
  if (consRefine)
    {
      BOOST_AUTO (labelMapPM, make_assoc_property_map (labelMap));
      res = dagItpComp.dagItp (rArg, topo, envMapPM, 
			       argCondPM, labelMapPM, labelsPM);
    }
  else
    {
      static_property_map<Expr> smap(mk<TRUE> (efac));
      res = dagItpComp.dagItp (rArg, topo, envMapPM, 
			       argCondPM, smap, labelsPM);
    }
      
  Stats::stop ("refiner.dag.interpolate");

  if (res)
    {
	  
      assert (labels.size () == topo.size ());

      Stats::resume ("refiner.nnf");	  
      // -- put labels into NNF (required by other passes)
      forall (Expr &v, labels | boost::adaptors::map_values)
	{
	  if (ufocl::UFO_REFINER_USE_ABC)
	    v = abc_simplify (v);
	  else
	    v = boolop::gather (boolop::nnf (v));
	}	  
      Stats::stop ("refiner.nnf");	  	  

      // disabled old strengthening code. Will remove soon.
#ifdef OLD_STRENGTHEN
      LOG ("ditp_verbose",
	   dbgs () << "BEFORE STRENGTHEN\n";
	   forall (NodeExprMap::value_type &kv, labels)
	   dbgs () << kv.first->getId () << ": " << *kv.second << "\n";);
	  

      Stats::resume ("refiner.strengthen");
      if (consRefine)
	{
	  BOOST_AUTO (labelMapPM, make_assoc_property_map (labelMap));
	  strengthen (rArg, topo, argCondPM, envMapPM, 
		      labelMapPM, labelsPM);
	}
      else
	{
	  static_property_map<Expr> smap(mk<TRUE> (efac));
	  strengthen (rArg, topo, argCondPM, envMapPM, 
		      smap, labelsPM);
	}
      Stats::stop ("refiner.strengthen");
#endif

      LOG ("ditp_verbose",
	   dbgs () << "AFTER STRENGTHEN\n";
	   forall (NodeExprMap::value_type &kv, labels)
	   dbgs () << kv.first->getId () << ": " << *kv.second << "\n";);

	  
      forall (Expr &v, labels | boost::adaptors::map_values)
	{
	  StripVariants sv;
	  v = replace (v, mk_fn_map (sv));
	}


      if (consRefine)
	{
	  forall (NodeExprMap::value_type &kv, labels)
	    {
	      if (isLeaf (kv.first, rArg) || 
		  isRoot (kv.first, rArg)) continue;
		  
	      NodeExprMap::const_iterator it = nodeLabels.find (kv.first);
	      assert (it != nodeLabels.end ());
	      kv.second = boolop::land (kv.second, it->second);
	    }
	}  

      assert (isOpX<TRUE> (labels [entryN]));
      assert (isOpX<FALSE> (labels [exitN]));
    }
  else labels.clear ();

  Stats::stop ("refiner.dag.interpolate.total");
}
