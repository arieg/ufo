#ifndef __EXPR_VISIT_H_
#define __EXPR_VISIT_H_

#include "ufo/EdgeComp.hpp"

namespace ufo
{

  /** Collects assumptions appearing in expression */
  struct collectAssumptions
  {
    ExprVector& assumptions;

    VisitAction operator() (Expr exp) 
    { 
      if (isOpX<ASM>(exp)){
        // if (isOpX<NODE>(exp->left()))
        //   assumptions.insert(assumptions.begin(), exp);
        // else 
        assumptions.push_back(exp);
      }

      return VisitAction::doKids (); 
    }

    collectAssumptions(ExprVector& assum):
      assumptions(assum) {}
  };

  struct addAssumptions
  {
    VisitAction operator() (Expr exp) const
    { 
      if (!(isOpX<AND>(exp) || isOpX<OR>(exp) || isOpX<IMPL>(exp)
            || isOpX<XOR>(exp)))
        return VisitAction::changeTo (mk<IMPL>(mk<ASM>(exp), exp));

      return VisitAction::doKids (); 
    }

    addAssumptions()  {}
  };

  struct PlaceAssumptions 
  {
    Expr operator() (Expr exp) const
    {
      Expr res;
      // -- do nothing for AND and OR. 
      if (isOpX<AND> (exp) || isOpX<OR> (exp)) return Expr();

      // -- otherwise, place an assumption for the current subformula
      return mk<IMPL> (mk<ASM> (exp), exp);
    }

  };


  /** replaces assumptions in trueAssumptions 
    with true and others to false 
   */
  struct replaceAssumptions
  {
    ExprSet& trueAssumptions;

    Expr trueE;
    Expr falseE;

    VisitAction operator() (Expr exp) const
    { 
      if (isOpX<ASM>(exp)){
        if (trueAssumptions.count(exp) > 0)
          return VisitAction::changeTo(trueE);
        else
          return VisitAction::changeTo(falseE);
      }

      return VisitAction::doKids (); 
    }

    replaceAssumptions(ExprSet& assumSet, Expr t, Expr f): 
      trueAssumptions(assumSet), trueE(t), falseE(f) {}

  };

  struct KillAssumptions 
  {
    const ExprSet &trueA;

    KillAssumptions (const ExprSet &good) : trueA (good) {}
    KillAssumptions (const KillAssumptions &other) : 
      trueA(other.trueA), 
      cachedFalseE (other.cachedFalseE), cachedTrueE (other.cachedTrueE) {}
    
    

    Expr operator() (Expr exp) const
    {
      Expr res(0);
      // -- if ASM, either change it to false, or leave unchanged
      if (isOpX<ASM> (exp)) 
        res = trueA.count (exp) <= 0 ? falseE (exp) : trueE (exp);

      // -- don't descend into non-boolean parts of the expression
      else if (!isOp<BoolOp> (exp) && !isOpX<TUPLE> (exp)) res = exp;

      // -- return null to indicate that children must be processed
      return res;
    }


    mutable Expr cachedFalseE;
    Expr falseE (Expr exp) const
    {
      if (!cachedFalseE) cachedFalseE = mk<FALSE> (exp->efac ());
      return cachedFalseE;
    }

    mutable Expr cachedTrueE;
    Expr trueE (Expr exp) const
    {
      if (!cachedTrueE) cachedTrueE = mk<TRUE> (exp->efac ());
      return cachedTrueE;
    }


  };

  struct TurnAssumptionsFalse 
  {
    const ExprSet &skip;

    TurnAssumptionsFalse (const ExprSet &trueAssume) : skip (trueAssume) {}

    Expr operator() (Expr exp) const
    {
      Expr res;
      // -- if ASM, either change it to false, or leave unchanged
      if (isOpX<ASM> (exp)) 
        res = skip.count (exp) <= 0 ? falseE (exp) : exp;

      // -- don't descend into non-boolean parts of the expression
      else if (!isOp<BoolOp> (exp)) res = exp;

      // -- return null to indicate that children must be processed
      return res;
    }

    mutable Expr cachedFalseE;
    Expr falseE (Expr exp) const
    {
      if (!cachedFalseE) cachedFalseE = mk<FALSE> (exp->efac ());
      return cachedFalseE;
    }
  };


  /** replaces assumptions not in trueAssumptions
    with false
   */
  struct replaceAssumptionsFalse
  {
    ExprSet& trueAssumptions;

    Expr trueE;
    Expr falseE;

    VisitAction operator() (Expr exp) const
    { 
      if (isOpX<ASM>(exp)){
        if (trueAssumptions.count(exp) <= 0)
          return VisitAction::changeTo(falseE);
      }

      return VisitAction::doKids (); 
    }

    replaceAssumptionsFalse(ExprSet& assumSet, Expr t, Expr f): 
      trueAssumptions(assumSet), trueE(t), falseE(f) {}

  };

  /** replaces assumptions not in trueAssumptions
    with false if they are not in nodes 
   */
  struct replaceAssumptionsFalse_NotNode
  {
    ExprSet& trueAssumptions;

    Expr trueE;
    Expr falseE;

    VisitAction operator() (Expr exp) const
    { 
      if (isOpX<ASM>(exp)){
        if (trueAssumptions.count(exp) <= 0 && !isOpX<NODE>(exp->left()))
          return VisitAction::changeTo(falseE);
      }

      return VisitAction::doKids (); 
    }

    replaceAssumptionsFalse_NotNode(ExprSet& assumSet, Expr t, Expr f): 
      trueAssumptions(assumSet), trueE(t), falseE(f) {}

  };


  // -- takes a formula and updates variables as per env
  struct updateFormula
  {
    // -- Environment passed
    Environment& env;

    VisitAction operator() (Expr exp) const
    { 
      // -- in case a variable (not a constant)
      if (isOpX<BB>(exp) || 
          isOpX<VALUE>(exp) || 
          isOpX<NODE>(exp) || 
          isOpX<VARIANT> (exp)){
        return VisitAction::changeTo(env.lookup(exp));
      }
      return VisitAction::doKids (); 
    }

    updateFormula(Environment& e): env(e) {}
  };    




  struct SetToConst{
    Environment* env;
    Expr falseE;
    Expr zeroE;
    bool outOfScope;
    VisitAction operator() (Expr exp)
    {
      if (isOpX<VARIANT>(exp))
      {

        Expr mv = variant::mainVariant(exp);

        if (env->has_binding (mv, exp)) return VisitAction::skipKids();


        if (isOpX<BB>(mv)) 
          return VisitAction::changeTo(falseE);

        // -- mv could be primed
        if (isOpX<VARIANT> (mv)) 
        { 
          mv = variant::mainVariant (mv); 
          // -- need to check if bound again
          if (env->has_binding (mv, exp)) 
            return VisitAction::skipKids ();
        }

        outOfScope = true;	    

        const Value* c = 
          dynamic_cast<const VALUE&> (mv->op ()).get ();

        assert (c != NULL && "can't be null");

        return  (c->getType ()->isIntegerTy (1)) ?
          VisitAction::changeTo(falseE) :
          VisitAction::changeTo(zeroE);
      }
      return VisitAction::doKids();
    }

    SetToConst(Environment* e, Expr t, Expr o) : 
      env(e), falseE(t), zeroE(o), outOfScope(false) {}
  };







}
#endif
