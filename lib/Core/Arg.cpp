#include "ufo/ArgBgl.hpp"
#include "ufo/ufo_graph.hpp"
#include <boost/graph/filtered_graph.hpp>

using namespace ufo;
ReachARG ufo::mk_reachable (ARG &arg, Node* entryN, Node* exitN, 
			    NodeSet &reach)
{
  using namespace arg_bgl_detail;
    
  NodeSetPM reachablePM (ufo::make_set_property_map (reach));
  // -- no-false-edge ARG
  typedef filtered_graph<ARG, NotFalseEdgePredicate<ARG> > NfeARG;
  NfeARG nfeArg =
    make_filtered_graph (arg, NotFalseEdgePredicate<ARG> (arg));
  ufo::slice (nfeArg, entryN, exitN, reachablePM);
    
  return make_filtered_graph 
    (arg, make_vertex_edge_predicate (arg, reachablePM), 
     make_vertex_predicate (arg, reachablePM));
}
