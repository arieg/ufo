#ifndef __ZPDR_DAG_INTERPOLATOR_HPP_
#define __ZPDR_DAG_INTERPOLATOR_HPP_

#include <boost/range.hpp>
#include <boost/range/algorithm/transform.hpp>
#include <boost/tuple/tuple.hpp>
#include <boost/graph/graph_traits.hpp>
#include <boost/property_map/property_map.hpp>

#include "ufo/ufo.hpp"
#include "ufo/Expr.hpp"
#include "ufo/Smt/UfoZ3.hpp"

#include "ufo/ufo_graph.hpp"

namespace ufo
{
  template <typename Graph>
  class ZPdrDagInterpolator
  {
  public:
    typedef graph_traits<Graph> Traits;
    typedef typename Traits::vertex_descriptor Vertex;
    typedef typename Traits::edge_descriptor Edge;
    
  private:
    ExprFactory &m_efac;
    CptLiveValues &m_live;
    
    UfoZ3 m_z3;
    ZFixedPoint<UfoZ3> m_fp;

    /** Map from nodes to relations */
    NodeExprMap decls;
    ufo::IsVar m_IsVar;
    

  public:
    ZPdrDagInterpolator (ExprFactory &efac, CptLiveValues &live) : 
      m_efac (efac), m_live (live), m_z3 (m_efac), m_fp (m_z3) 
    {
      ZParams<UfoZ3> params (m_z3);
      params.set (":engine", "pdr");
      params.set (":use-farkas", true);
      params.set (":generate-proof-trace", false);
      params.set (":slice", false);
      params.set (":inline-linear", false);
      params.set (":inline-eager", false);

      m_fp.set (params);
    }
    

    /** Computes a DAG interpolant.
     * 
     * \tparam Range a boost::range
     * \tparam EdgeMap a property map frome Edges to Expr
     * \tparam VertexMap a property map from Vertecies to Expr
     * 
     * \param[in] g the input graph
     * \param[in] topo topological order of vertices of g
     * \param[in] envMap a map from verticies of g to Environment at the vertex
     * \param[in] argCond a property map from edges of g to Expr
     * \param[in] lemmas optional map from verticies to assumed lemmas
     * \param[out] out DAG interpolant as a vertex map
     * 
     * \return true if there is an interpolant, false if the formula is
     * satisfiable, and indeterminate if SMT-solver failed to decide the
     * formula.
     */
    template <typename Range, typename EnvMap, 
	      typename EdgeMap, typename VertexLabelMap, typename VertexMap>
    boost::tribool dagItp (const Graph &g, Range &topo, 
			   const EnvMap &envMap, 
			   const EdgeMap &argCond, 
			   const VertexLabelMap &lemmas,
			   VertexMap &out)
    {
      Node *exitN = NULL;
      
      forall (Node* v, topo)
	{
	  Expr vDecl = declare (v);
	  m_fp.registerRelation (vDecl);

	  ExprVector vVars (boost::size (varsOf (v)));
	  boost::transform (varsOf (v), vVars.begin (), get (envMap, v));
	  Expr vApp = bind::fapp (vDecl, vVars);
	  
	  if (isRoot (v, g)) 
	    {
	      // -- ROOT <- true
	      assert (vVars.empty ());
	      m_fp.addRule (vVars, vApp);
	      continue;
	    }

	  if (isLeaf (v, g)) exitN = v;

	  Expr lemma = get (lemmas, v);

	  // XXX addCover() does not currently work with
	  // XXX assumptions. Add them directly into the rule
	  // if (lemma &&
	  // !isOpX<TRUE> (lemma)) { LOG("ufo_verbose", dbgs () <<
	  // "Node: " << v->getId () << " adding: " << *lemma <<
	  // "\n";); m_fp.addCover (bind::fapp (vDecl, varsOf (v)),
	  // lemma);
	    // }

	  
	  forall (Edge uv, boost::in_edges (v, g))
	    {
	      Vertex u = boost::source (uv, g);
	      Expr uDecl = declOf (u);

	      ExprVector uVars (boost::size (varsOf (u)));
	      boost::transform (varsOf (u), uVars.begin (), get (envMap, u));
	      Expr uApp = bind::fapp (uDecl, uVars);
	      
	      Expr body = get (argCond, uv);
	      if (isOpX<TUPLE> (body)) 
		body = boolop::land (body->left (), body->right ());

	      if (lemma) body = boolop::land (body, lemma);

	      Expr rule = boolop::limp (boolop::land (uApp, body), vApp);
	      ExprVector allVars;
	      findVars (rule, std::back_inserter (allVars));
	      m_fp.addRule (allVars, rule);
	      dbgs () << "Adding rule: " << *boolop::pp(rule) << "\n";
	    }	  

	  
	  
	}

      assert (exitN != NULL);

      dbgs () << "FP BEGIN\n"
	      << m_fp.toString (bind::fapp (declOf (exitN))) << "\n"
	      << "FP END\n";

      // -- call query
      Stopwatch sw;
      boost::tribool res = m_fp.query (bind::fapp (declOf (exitN)));
      sw.stop ();
      
      errs () << "FP QUERY: time: " << sw << "\n";
      errs ().flush ();
      dbgs () << "ANSWER: " << m_fp.getAnswer () << "\n";
      
      // -- collect all lemmas
      if (!res)
	{
	  forall (Node* v, topo)
	    {
	      Expr vDecl = declOf (v);
	      Expr lemma = m_fp.getCoverDelta (bind::fapp (vDecl, varsOf(v)));
	      LOG("zpdr", 
		  dbgs () << "LEMMA: " << v->getId () << " " << *lemma << "\n";);
	      boost::put (out, v, lemma);
	    }
	}
      
      // XXX return true if refinement was succsessful, false if it
      // was not, and unknown otherwise
      return !res;
      
    }

  private:

    template <typename OutputIterator>
    void findVars (Expr rule, OutputIterator out)
    {
      filter (rule, m_IsVar, out);
    }
    
    const ExprVector &varsOf (const Node *v) 
    { return m_live.live (v->getLoc ()); }
    Expr declare (Node *n) 
    {
      Expr res = declOf (n);
      if (!res) 
	{
	  ExprVector sig;
	  forall (Expr var, varsOf (n))
	    sig.push_back (ufo::typeOf (var));
	  sig.push_back (mk<BOOL_TY> (m_efac));


          std::string name = "R" + boost::lexical_cast<std::string>(n->getId ()) + "_" + 
	    n->getLoc ()->getBB ()->getNameStr ();
	  
	  res = bind::fdecl (mkTerm (name, m_efac), sig);
	  decls [n] = res;

	  // -- mark nullary relations as non-constants
	  if (sig.size () == 1) m_IsVar.exception (bind::fapp (res));
	}

	return res;      
    }
    Expr declOf (Node *n) const
    {
      NodeExprMap::const_iterator it = decls.find (n);
      if (it != decls.end ()) return it->second;
      return Expr (0);
    }
    
    
  };
    
}


#endif
