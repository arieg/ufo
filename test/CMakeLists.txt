set (UFO_TEST_DIRS "Tacas12")

set (LLVM_LIT ${LLVM_ROOT}/bin/llvm-lit)
set (LLVM_SOURCE_DIR ${LLVM_ROOT})
set (LLVM_BINARY_DIR ${LLVM_ROOT})
set (LLVM_TOOLS_DIR ${LLVM_ROOT}/bin)
set (LLVM_LIBS_DIR ${LLVM_ROOT/lib})
set (UFO_LIB_DIR ${UFO_BINARY_DIR}/lib)

set (UFO_TEST_DEPS ufo)

             
set(PROJ_SRC_ROOT ${UFO_SOURCE_DIR})     
set(PROJ_SRC_DIR ${CMAKE_CURRENT_SOURCE_DIR})
set(PROJ_OBJ_DIR ${CMAKE_CURRENT_BINARY_DIR})
set(PROJ_TOOLS_DIR ${UFO_BINARY_DIR}/tools/ufo)

include (FindPythonInterp)
if (PYTHONINTERP_FOUND)
  option (UFO_NO_PROGRESS_BAR "Disable progress bar" OFF)
  set (UFO_TEST_EXTRA_ARGS)
  if (XCODE OR UFO_NO_PROGRESS_BAR)
    set (UFO_TEST_EXTRA_ARGS "--no-progress-bar")
  endif()
  
  option (UFO_TEST_USE_VG "Run tests under vallgrind" OFF)
  if (UFO_TEST_USE_VG)
    set (UFO_TEST_EXTRA_ARGS ${UFO_EXTRA_ARGS} "--vg")
  endif()

  foreach (t ${UFO_TEST_DIRS})
    add_test (test-${t}
      ${LLVM_LIT} 
      --param ufo_site_config=${CMAKE_CURRENT_BINARY_DIR}/lit.site.cfg
      --param build_config=${CMAKE_CFG_INTDIR}
      -sv ${UFO_TEST_EXTRA_ARGS}
      ${CMAKE_CURRENT_BINARY_DIR}/${t})
  endforeach()

  
  foreach (t ${UFO_TEST_DIRS})
    add_custom_target (ufo-test-${t}
    COMMAND 
    ${LLVM_LIT} 
    --param ufo_site_config=${CMAKE_CURRENT_BINARY_DIR}/lit.site.cfg
    --param build_config=${CMAKE_CFG_INTDIR}
    -sv ${UFO_TEST_EXTRA_ARGS}
    ${CMAKE_CURRENT_BINARY_DIR}/${t}
    DEPENDS ${UFO_TEST_DEPS}
    COMMENT "Running UFO regressions in ${t}")
  endforeach()

  add_custom_target(check-ufo
    COMMAND 
    ${LLVM_LIT} 
    --param ufo_site_config=${CMAKE_CURRENT_BINARY_DIR}/lit.site.cfg
    --param build_config=${CMAKE_CFG_INTDIR}
    -sv ${UFO_TEST_EXTRA_ARGS}
    ${CMAKE_CURRENT_BINARY_DIR}
    DEPENDS ${UFO_TEST_DEPS}
    COMMENT "Running UFO regressions")
endif()

configure_file (${CMAKE_CURRENT_SOURCE_DIR}/lit.site.cfg.in
  ${CMAKE_CURRENT_BINARY_DIR}/lit.site.cfg)
configure_file (${CMAKE_CURRENT_SOURCE_DIR}/site.exp.in
  ${CMAKE_CURRENT_BINARY_DIR}/site.exp)
