#ifndef __UFO__ITERATOR__HPP_
#define __UFO__ITERATOR__HPP_
/** Utility functions for iterators */

#include <boost/iterator/zip_iterator.hpp>

namespace ufo
{
  using namespace boost;
  template <typename Iterator1, typename Iterator2>
  zip_iterator<boost::tuple<Iterator1, Iterator2 > > 
  mk_zip_it (Iterator1 it1, Iterator2 it2)
  {
    return make_zip_iterator (make_tuple (it1, it2));
  }

  template <typename Range1, typename Range2>
  std::pair<zip_iterator 
	    <boost::tuple <typename boost::range_iterator<Range1>::type,
			   typename boost::range_iterator<Range2>::type > >,
	    zip_iterator 
	    <boost::tuple <typename boost::range_iterator<Range1>::type,
			   typename boost::range_iterator<Range2>::type > > >
  mk_zip_rng (const Range1 &r1, const Range2 &r2)
  {
    return make_pair (mk_zip_it (begin (r1), begin (r2)),
		      mk_zip_it (end (r1), end (r2)));
  }

}


#endif
