#ifndef _UFO_Z3__HPP_
#define _UFO_Z3__HPP_
/** UFO specific interface to Z3 */

#include "ZExprConverter.hpp"

#include "ufo/ufo.hpp"
// -- defines NODE
#include "ufo/Arg.hpp"

namespace ufo
{

  /** UFO specific Z3 marshaler */
  template <typename M>
  struct UfoExprMarshal
  {
    template <typename C>
    static z3::ast marshal (Expr e, z3::context &ctx, C &cache, 
			    expr_ast_map &seen)
    {
      if (isOpX<ASM>(e))
	{
	  Expr assum = e->left();
	  std::string svar;
	  
	  if(isOpX<ULONG>(assum))
            svar = "assum" + 
	      boost::lexical_cast<std::string>(assum.get());
	  else if (isOpX<NODE>(assum))
	    svar = "node_assum"  +
	      boost::lexical_cast<std::string,void*>(e.get());
	  else
	    svar = "arie_eq_assum"  +
	      boost::lexical_cast<std::string,void*>(e.get());
	  //assert (0 && "incorrect assumption");

	  z3::sort sort (ctx.bool_sort ());
	  z3::symbol symname ( ctx.str_symbol (svar.c_str ()));
	  z3::ast res (ctx, Z3_mk_const (ctx, symname, sort));
	  
	  // -- update reverse lookup map
	  cache.insert (typename C::value_type (e.get (), res));
	  return res;
	}

      if (isOpX<VARIANT>(e))
	{
	  int num = variant::variantNum(e);
	  Expr var = variant::mainVariant(e);
	  std::string svar;
	  if (ufocl::UFO_Z3_PRETTY)
	    svar = "v" + boost::lexical_cast<std::string>(*var);
	  else
	    svar = "v" + boost::lexical_cast<std::string,void*>(var.get());
	  svar += "_" + boost::lexical_cast<std::string>(num);

                        
	  z3::sort sort (ctx);
	  if (isOpX<BB> (var))
	    sort = ctx.bool_sort ();
	  else
	    {
	      Expr exp = var;
		  
	      if (isOp<VARIANT>(var)) exp = variant::mainVariant(var);
		  
	      const Value* c = getTerm<const Value*> (exp);
		  
	      assert (c != NULL && "can't be null");
 
	      if (c->getType ()->isIntegerTy (1))
		sort = ctx.bool_sort ();
	      else
		sort = ufocl::USE_INTS ? ctx.int_sort () : ctx.real_sort ();
	    }
	    
	  z3::symbol symname (ctx.str_symbol (svar.c_str ()));
	  z3::ast res (ctx, Z3_mk_const (ctx, symname, sort));
	  
	  cache.insert (typename C::value_type (e.get (), res));
	  return res;
	}

	// -- Symbolic Constant
      if (isOpX<VALUE>(e))
	{
	  std::string svar;
	  if (ufocl::UFO_Z3_PRETTY)
	    svar = "x" + boost::lexical_cast<std::string>(*e);
	  else 
	    svar = "x" + boost::lexical_cast<std::string,void*>(e.get());

	  const Value* c = getTerm<const Value*> (e);

	  assert (c != NULL && "can't be null");

	  z3::sort sort (ctx);
	  if (c->getType ()->isIntegerTy (1))
	    sort = ctx.bool_sort ();
	  else  
	    sort = ufocl::USE_INTS ? ctx.int_sort () : ctx.real_sort ();
	  
	  z3::symbol symname (ctx.str_symbol (svar.c_str ()));
	  z3::ast res (ctx, Z3_mk_const (ctx, symname, sort));
	  cache.insert (typename C::value_type (e.get (), res));
	  return res;
	}
      
      if (isOpX<BB>(e) || isOpX<NODE>(e))
	{
	  std::string svar = (isOpX<BB>(e) ? "BB" : "N" );
	  
	  if (ufocl::UFO_Z3_PRETTY)
	    svar += boost::lexical_cast<std::string>(*e);
	  else
	    svar += boost::lexical_cast<std::string,void*>(e.get());
        
	  z3::sort sort (ctx.bool_sort ());
	  z3::symbol symname (ctx.str_symbol (svar.c_str ()));
	  z3::ast res (ctx, Z3_mk_const (ctx, symname, sort));

	  cache.insert (typename C::value_type (e.get (), res));
	  return res;
	}
      
      return M::marshal (e, ctx, cache, seen);
    }
    
    
  };

  /** UfoZ3 represents the context */
  // -- note that no need to unmarshal UFO expressions since they are cached
  typedef ZContext<BasicExprMarshal< UfoExprMarshal<FailMarshal> >,
		   BasicExprUnmarshal< FailUnmarshal> > UfoZ3;    

  /** backward compatibility solver class that includes the context */
  class UfoCompSolver
  {
  private:
    UfoZ3 m_ctx;
    ZSolver<UfoZ3> m_solver;
  public:
    UfoCompSolver (ExprFactory &efac) : m_ctx (efac), m_solver (m_ctx) {}
    
    void assertExpr (Expr e) { m_solver.assertExpr (e); }
    boost::tribool solve () { return m_solver.solve (); }
    void push () { m_solver.push (); }
    void pop (unsigned n = 1) { m_solver.pop (n); }
    void reset () { m_solver.reset (); }
  };

  inline boost::tribool z3n_is_sat (Expr e)
  {
    UfoZ3 z3 (e->efac ());
    return z3_is_sat<UfoZ3> (z3, e);
  }

  inline std::string z3n_to_smtlib (Expr e)
  {
    UfoZ3 z3 (e->efac ());
    return z3_to_smtlib<UfoZ3> (z3, e);
  }

  inline Expr z3n_lite_simplify (Expr e)
  {
    UfoZ3 z3 (e->efac ());
    return z3_lite_simplify (z3, e);
  }
  
  
  
  
}


#endif
