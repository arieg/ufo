#ifndef __UFO_ARG_BGL__HPP_
#define __UFO_ARG_BGL__HPP_

/** BGL interface to ARG */

#include "Arg.hpp"
#include <boost/graph/graph_traits.hpp>
#include <boost/graph/properties.hpp>
#include <boost/iterator/transform_iterator.hpp>
#include <boost/property_map/property_map.hpp>
#include "ufo/property_map.hpp"

namespace boost
{
  // -- forward declaration of filtered graph
  template <typename Graph, 
	    typename EdgePredicate, 
	    typename VertexPredicate> class filtered_graph;
}

namespace ufo
{
  /** Helper classes for transform_iterator */
  struct MkOutEdgePair : public std::unary_function<Node*,NodePair>
  {
    Node *src;
    
    MkOutEdgePair () : src(NULL) {}
    MkOutEdgePair (Node *u) : src(u) {} 
    NodePair operator() (Node *v) const 
    { assert (src); return std::make_pair (src, v); }
  };

  struct MkInEdgePair : public std::unary_function<Node*,NodePair>
  {
    Node *dst;
    
    MkInEdgePair () : dst(NULL) {}
    
    MkInEdgePair (Node *v) : dst(v) {} 
    NodePair operator() (Node *u) const 
    { assert (dst); return std::make_pair (u, dst); }
  };  


  struct false_edge_prop_t {};

  struct FalseEdgePropertyMap
  {
    typedef bool value_type;
    typedef bool reference;
    typedef NodePair key_type;
    typedef readable_property_map_tag category;

    const ARG &arg;
    FalseEdgePropertyMap (const ARG &g) : arg(g) {}
  };

  struct NodeIndexPropertyMap 
  {
    typedef size_t value_type;
    typedef value_type& reference;
    typedef Node* key_type;
    typedef readable_property_map_tag category;
  };
}
namespace boost
{
  
  inline bool get (FalseEdgePropertyMap pm, NodePair edg)
  {
    return pm.arg.isFalseEdge (edg.first, edg.second);
  }  
  
  inline size_t get (NodeIndexPropertyMap pm, Node *n)
  {
    return n->getId ();
  }
  

  /** BGL graph interface */
  using namespace ufo;
  
  template <>
  struct graph_traits<ARG>
  {
    /** vertex and edge types */
    typedef Node* vertex_descriptor;
    typedef NodePair edge_descriptor;
    
    /** tags */
    typedef disallow_parallel_edge_tag edge_parallel_category;
    typedef bidirectional_tag directed_category;
    struct arg_graph_tag : 
      virtual bidirectional_graph_tag, virtual vertex_list_graph_tag {};
    typedef arg_graph_tag traversal_category;

    /** sizes */
    typedef size_t vertices_size_type;
    typedef size_t edges_size_type;
    typedef size_t degree_size_type;

    /** iterators */
    typedef transform_iterator<MkOutEdgePair, NodeVector::const_iterator>
    out_edge_iterator;
    
    typedef transform_iterator<MkInEdgePair, NodeVector::const_iterator>
    in_edge_iterator;

    typedef NodeVector::const_iterator vertex_iterator;

    /** unimplemented iterator over edges to make filtered_graph happy */
    typedef in_edge_iterator edge_iterator;

    static vertex_descriptor null_vertex () { return NULL; }    
  };
  
  inline graph_traits<ARG>::vertex_descriptor source 
  (graph_traits<ARG>::edge_descriptor e, const ARG &g)
  {
    return e.first;
  }

  inline graph_traits<ARG>::vertex_descriptor target
  (graph_traits<ARG>::edge_descriptor e, const ARG &g)
  {
    return e.second;
  }
  
  
  inline std::pair<graph_traits<ARG>::out_edge_iterator, 
		   graph_traits<ARG>::out_edge_iterator > 
  out_edges
  (graph_traits<ARG>::vertex_descriptor u, const ARG &g)
  {
    return make_pair 
      (make_transform_iterator (u->begin (), MkOutEdgePair (u)),
       make_transform_iterator (u->end (), MkOutEdgePair (u)));
  }
  
  inline graph_traits<ARG>::degree_size_type 
  out_degree
  (graph_traits<ARG>::vertex_descriptor u, const ARG &g)
  {
    return u->outDegree ();
  }

  inline std::pair<graph_traits<ARG>::in_edge_iterator, 
		   graph_traits<ARG>::in_edge_iterator > 
  in_edges
  (graph_traits<ARG>::vertex_descriptor u, const ARG &g)
  {
    return make_pair 
      (make_transform_iterator (u->pbegin (), MkInEdgePair (u)),
       make_transform_iterator (u->pend (), MkInEdgePair (u)));
  }


  inline graph_traits<ARG>::degree_size_type 
  in_degree
  (graph_traits<ARG>::vertex_descriptor u, const ARG &g)
  {
    return u->inDegree ();
  }
  

  inline graph_traits<ARG>::degree_size_type 
  degree
  (graph_traits<ARG>::vertex_descriptor u, const ARG &g)
  {
    return u->inDegree () + u->outDegree ();
  }

  inline std::pair<graph_traits<ARG>::vertex_iterator,
		   graph_traits<ARG>::vertex_iterator >
  vertices (const ARG &g)
  {
    return make_pair (g.begin (), g.end ());
  }

  inline graph_traits<ARG>::vertices_size_type 
  num_vertices (const ARG &g)
  {
    return g.size ();
  }

  template <>
  struct property_map<ARG, ufo::false_edge_prop_t>
  {
    typedef ufo::FalseEdgePropertyMap type;
    typedef type const_type;
  };
  
  inline ufo::FalseEdgePropertyMap get (ufo::false_edge_prop_t p, const ARG &g)
  {
    return FalseEdgePropertyMap (g);
  }

  inline bool get (ufo::false_edge_prop_t p, const ARG &g, 
	    graph_traits<ARG>::edge_descriptor edg)
  {
    return g.isFalseEdge (source (edg, g), target (edg, g));
  }    

  template <>
  struct property_map<ufo::ARG, boost::vertex_index_t>
  {
    typedef ufo::NodeIndexPropertyMap type;
    typedef type const_type;
  };
    
  inline ufo::NodeIndexPropertyMap get (boost::vertex_index_t p, const ARG &g)
  {
    return NodeIndexPropertyMap ();
  }
  inline size_t get (boost::vertex_index_t p, const ARG &g, Node* n)
  {
    return n->getId ();
  }
  
  
}

namespace ufo
{
  template<typename Graph>
  struct NotFalseEdgePredicate : 
    public std::unary_function<typename graph_traits<Graph>::edge_descriptor, 
			       bool>
  {
    typedef NotFalseEdgePredicate<Graph> this_type;
    typedef graph_traits<Graph> Traits;
    typedef typename Traits::edge_descriptor Edge;
    
    const Graph *g;
    
    NotFalseEdgePredicate () : g(NULL) {}
    
    NotFalseEdgePredicate (const Graph &graph) : g(&graph) {}
    NotFalseEdgePredicate (const this_type &other) : g(other.g) {}
    
    
    bool operator() (Edge edg) const
    {
      assert (g);
      return !get (false_edge_prop_t(), *g, edg);
    }

    this_type &operator= (const this_type &other)
    {
      this_type tmp (other);
      g = tmp.g;
      return *this;
    }
  };

  template <typename Graph, typename VertexMap>
  struct VertexPredicate : 
    public std::unary_function<typename graph_traits<Graph>::vertex_descriptor,
			       bool>
  {
    typedef VertexPredicate<Graph,VertexMap> this_type;
    typedef graph_traits<Graph> Traits;
    typedef typename Traits::vertex_descriptor Vertex;

    VertexMap vm;
    
    VertexPredicate () {}
    VertexPredicate (const VertexMap m) : vm (m) {}
    VertexPredicate (const this_type &other) : vm (other.vm) {}

    bool operator () (Vertex v) const { return get (vm, v); }
    this_type &operator= (const this_type &other)
    {
      this_type tmp (other);
      // XXX use std::swap
      vm = tmp.vm;
      return *this;
    }
    

  };
  
  template <typename Graph, typename VertexMap>
  VertexPredicate<Graph,VertexMap> make_vertex_predicate (const Graph &g, 
							  VertexMap m)
  {
    return VertexPredicate<Graph,VertexMap> (m);
  }
  
  template <typename Graph, typename VertexMap>
  struct VertexEdgePredicate : 
    public std::unary_function<typename graph_traits<Graph>::edge_descriptor, 
			       bool>
  {
    typedef VertexEdgePredicate<Graph,VertexMap> this_type;
    typedef graph_traits<Graph> Traits;
    typedef typename Traits::edge_descriptor Edge;

    const Graph *g;
    VertexMap vm;
    
    VertexEdgePredicate () : g(NULL) {}
    VertexEdgePredicate (const Graph &graph, VertexMap m) : 
      g(&graph), vm(m) {}
    VertexEdgePredicate (const this_type &other) : g(other.g), vm(other.vm) {}
    

    bool operator() (Edge edg) const
    {
      assert (g);
      return get (vm, target (edg, *g)) && get (vm, source (edg, *g));
    }

    this_type &operator= (const this_type &other)
    {
      this_type tmp (other);
      g = tmp.g;
      vm = tmp.vm;
      return *this;
    }
  };
    
  template <typename Graph, typename VertexMap>
  VertexEdgePredicate<Graph,VertexMap> make_vertex_edge_predicate 
  (const Graph &g, VertexMap m)
  {
    return VertexEdgePredicate<Graph,VertexMap> (g, m);
  }  

  namespace arg_bgl_detail
  {
    typedef SetPropertyMap<NodeSet> NodeSetPM;    
    typedef VertexEdgePredicate<ARG, NodeSetPM> VEPred;
    typedef VertexPredicate<ARG, NodeSetPM> VPred;
  }
  
  // -- ARG restricted to entry-exit reachable locations
  typedef boost::filtered_graph<ARG,
				arg_bgl_detail::VEPred,
				arg_bgl_detail::VPred> ReachARG;
  
  /***
      \brief Restricts the given ARG to vertices on the path between
      entry and exit locations. The reachable locations are stored in
      the provided set reach. The output graph references both the
      input ARG and the set reach.
   */
  ReachARG mk_reachable (ARG &arg, Node* entryN, Node* exitN, 
			 NodeSet &reach);
  
  
}

// must come after boost::graph_traits<ufo::ARG> has been defined
#include "boost/graph/filtered_graph.hpp"

#endif 
