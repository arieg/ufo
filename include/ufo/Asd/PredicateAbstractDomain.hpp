#ifndef __PRED_ABS_H_
#define __PRED_ABS_H_

#include "ufo/ufo.hpp"
#include "ufo/Asd/AbstractStateDomain.hpp"

#include "ufo/Smt/UfoZ3.hpp"

#include <list>
#include <boost/range/adaptor/transformed.hpp>
#include <boost/range/adaptor/filtered.hpp>
#include <boost/range/algorithm/transform.hpp>


// -- Predicate Abstraction
namespace ufo
{
  
  namespace paasd
  {    
    struct OverPredicates
    {
      VisitAction operator() (Expr exp) const
      { 
	if (bind::isBoolVar (exp)) 
	  return VisitAction::changeTo (bind::name (exp));
	return VisitAction::doKids ();
      }
    };
    
      

    // -- This visitor collects predicates from a given formula
    // -- and adds them to preds;
    template <typename T>
    struct CollectPredicates
    {
      CutPointPtr cp;
      BoolExprFn &filter;
      
      T &predabs;

      CollectPredicates(CutPointPtr c, BoolExprFn &f, T &p): 
	cp(c), filter(f), predabs(p) {}

      VisitAction operator() (Expr exp)
      {
	if (isOpX<TRUE> (exp) || isOpX<FALSE> (exp)) 
	  return VisitAction::skipKids ();
	
	if (isOp<BoolOp> (exp)) return VisitAction::doKids();

	if (filter.apply (exp)) predabs.addPred (cp, exp);

	return VisitAction::skipKids ();
      }
    };

  }

  using namespace ufo;

  class PredicateAbstractStateValue : public AbstractStateValue
  {
  protected:
    Expr val;

  public:
    PredicateAbstractStateValue (Expr v) : val(v) {}
    
    Expr getVal () const { return val; }
  };
    

  /** Base for Predicate Abstraction. A predicate abstraction is a map
      from CutPoints to Predicates and a Post Image function*/
  class PredicateAbstractDomain : public AbstractStateDomain
  {
  protected:
    /** Large Block Encoding */
    LBE& lbe;

    ExprFactory& efac;

    // -- helper constants
    Expr trueE;
    Expr falseE;
      
    // -- for now will assume only MSAT is passable
    ExprMSat& ms;			

    BoolExprFn *filter;

  public:
    PredicateAbstractDomain (LBE& lbePass, ExprFactory& e, ExprMSat& m, 
			     BoolExprFn *f = new TrueBoolExprFn ()) : 
      lbe(lbePass), efac(e), trueE(mk<TRUE>(efac)), falseE(mk<FALSE>(efac)), 
      ms(m), filter (f) {}

    virtual bool isFinite () { return true; }
    std::string name () { return "NULLPA"; }
    
    virtual AbstractState alpha (CutPointPtr loc, Expr val) 
    { 
      return absState (val); 
    }
    
    virtual Expr gamma (CutPointPtr loc, AbstractState v)
    {
      return getExpr (v);
    }

    virtual AbstractState top (CutPointPtr loc) 
    {
      return absState (trueE);
    }
    virtual AbstractState bot (CutPointPtr loc)
    {
      return absState (falseE);
    }

    virtual bool isTop (CutPointPtr loc, AbstractState v)
    {
      Expr e = getExpr (v);
      return e == trueE || isSAT (boolop::lneg (e)) == false;
    }

    virtual bool isBot (CutPointPtr loc, AbstractState v)
    {
      // BOT == UNSAT
      Expr e = getExpr (v);
      return e == falseE || isSAT (e) == false;
    }
    
    virtual bool isLeq (CutPointPtr loc, AbstractState v1, AbstractState v2)
    {
      Expr e1 = getExpr (v1);
      Expr e2 = getExpr (v2);
      return isSAT (boolop::land (e1, boolop::lneg (e2))) == false;
    }
    
    virtual bool isLeq (CutPointPtr loc, AbstractState v, 
			AbstractStateVector const &u)
    {
      Expr c = trueE;
      foreach (AbstractState s, u)
	c = boolop::land (c, boolop::lneg (getExpr (s)));
      
      Expr e = getExpr (v);
      return isSAT (boolop::land (e, c)) == false;
    }
    
    virtual bool isEq (CutPointPtr loc, AbstractState v1, AbstractState v2)
    {
      Expr e1 = getExpr (v1);
      Expr e2 = getExpr (v2);
      
      return isSAT 
	(boolop::lor (boolop::land (e1, boolop::lneg (e2)),
		      boolop::land (e2, boolop::lneg (e1)))) == false;
    }
    
    virtual AbstractState join (CutPointPtr loc, 
				AbstractState v1, AbstractState v2)
    {
      return absState (boolop::lor (getExpr (v1), getExpr (v2)));
    }

    virtual AbstractState meet (CutPointPtr loc, 
				AbstractState v1, AbstractState v2)
    {
      return absState (boolop::land (getExpr (v1), getExpr (v2)));
    }

    virtual AbstractState widen (CutPointPtr loc, 
				AbstractState v1, AbstractState v2)
    {
      return v2;
    }

    virtual AbstractState widenWith (CutPointPtr loc, 
				     const AbstractStateVector &newV,
				     AbstractState oldV)
    {
      return oldV;
    }
    
    
    virtual AbstractState post (AbstractState pre, 
				CutPointPtr src, CutPointPtr dst)
    {
      edgeIterator it = src->findSEdge (*dst);
      assert (it != src->succEnd ());
      
      return absState (alphaPost (getExpr (pre), *it));
    }

    virtual AbstractState post (const LocLabelStatePairVector &pre,
				Loc dst, 
				BoolVector &deadLocs)
    {
      Expr res = falseE;

      
      size_t count = 0;
      foreach (LocLabelStatePair lv, pre)
	{
	  edgeIterator it = lv.first->findSEdge (*dst);
	  assert (it != lv.first->succEnd ());
	  
	  Expr u = lv.second.first;
	  if (!u) u = getExpr (lv.second.second);
	  Expr x = alphaPost (u, *it);
	  
	  if (x == falseE) deadLocs [count] = true;
	  else res = boolop::lor (res, x);
	  count++;
	  
	} 

      return absState (res);
    }


    virtual void setRefinementFilter (BoolExprFn *f)
    {
      delete filter;
      filter = f;
    }
    
    virtual void refinementHint (CutPointPtr loc, Expr hint)
    {
      paasd::CollectPredicates<PredicateAbstractDomain> cp (loc, 
							     *filter, *this);
      // Light refinement:
      // CollectPredsFromInterp cpi (n->getLoc (), predDom); 
      
      dagVisit (cp, hint);
    }
    
  protected:
    AbstractState absState (Expr e)
    {
      return AbstractState (new PredicateAbstractStateValue (e));
    }

    Expr getExpr (AbstractState v)
    {
      return dynamic_cast<PredicateAbstractStateValue*>(&*v)->getVal ();
    }

  public:
    
    /** Returns the post image of pre over the edge */
    virtual Expr alphaPost (Expr pre, SEdgePtr edge) 
    { 
      return pre == falseE ? falseE : trueE; 
    }
		
    /** Adds a predicate to a cut point */
    void addPred (CutPointPtr cp, Expr p) { cp2preds[cp].insert (p); }

    /** Returns predicates for a given cutpoint */
    ExprSet& getPreds(CutPointPtr cp) { return cp2preds[cp]; }

    /** Forgets all predicates */
    void reset () { cp2preds.clear(); }

  private:
    /** map from CutPoints to Predicates */
    std::map<CutPointPtr,std::set<Expr> > cp2preds;

  protected:

    /** checks satisfiability of expression with the current theorem prover */
    boost::tribool isSAT(Expr e) //{ return ms.isSAT(e); }
    {
      return z3n_is_sat (e);
    }
    
  public:
    virtual ~PredicateAbstractDomain () { delete filter; }
  };

  class CartesianPADomain : public PredicateAbstractDomain
  {
  public:

    CartesianPADomain (LBE& l, ExprFactory& e, ExprMSat& m) : 
      PredicateAbstractDomain (l, e, m) {}

    std::string name () { return "CPRED"; }
    
    virtual Expr alphaPost (Expr pre, SEdgePtr edge)
    {

      Environment env (efac);
      Expr ant = SEdgeCondComp::computeEdgeCond (efac, env, pre, edge, lbe);
      Expr result = trueE;

      ExprSet& dstPreds = getPreds (edge->getDst ());

      if (dstPreds.empty())
	{
	  boost::tribool sat = isSAT(ant);
	  if (sat) 
	    return trueE;
	  else if (maybe(sat))
	    {
	      assert(0 && "\nMAYBE\n");
	      return trueE;
	    }
	  else 
	    return falseE;
	}
      
      // -- !q.preds.empty ()
      foreach (Expr p, dstPreds)
	{
	  // -- p in the environment
	  Expr envP = env.eval (p);
	 
	  if(!isSAT (mk<AND> (ant, boolop::lneg (envP))))
	    result = boolop::land (result, p);
	  else if(!isSAT (mk<AND> (ant, (envP))))
	    result = boolop::land (result, boolop::lneg (p));
	}
      return result;
    }

  };

  class BoolPADomain : public PredicateAbstractDomain
  {
  public:

    BoolPADomain (LBE& l, ExprFactory& e, ExprMSat& m) : 
      PredicateAbstractDomain (l, e, m) {}

    std::string name () { return "BPRED"; }

    virtual Expr alphaPost (Expr pre, SEdgePtr edge)
    {
      Environment env (efac);
      // -- predicate abstraction query
      Expr paq = SEdgeCondComp::computeEdgeCond (efac, env, pre, edge, lbe);

      ExprSet &dstPreds = getPreds (edge->getDst ());

      if (dstPreds.empty ())
	{
	  boost::tribool sat = isSAT(paq);
	  if (sat) return trueE;
	  else if (maybe(sat))
	    {
	      assert(0 && "\nMAYBE\n");
	      return trueE;
	    }
	  else return falseE;
	}
        
      ExprVector important;

      /** enumerate predicates: required for STRING encoding :( */
      ExprVector preds (dstPreds.begin (), dstPreds.end ());
	
      for(size_t i = 0; i < preds.size (); i++)
	{
	  Expr pred = bind::boolVar (preds[i]);
	  Expr pdef = mk<IFF>(env.eval (preds [i]), pred);
	  paq = mk<AND>(paq, pdef);
          important.push_back(pred);
        }
              
      Expr result = allSAT (paq, important);

      // -- unwrap the result to be over original destination predicates
      paasd::OverPredicates op;
      return dagVisit (op, result);
    }
  private:
    Expr allSAT (Expr e, const ExprVector &terms)
    {
      return ms.allSAT (e, terms);
      //return z3_all_sat (e, terms);
    }
    
  };

  /** only picks expressions that correspond to intervals */
  struct IntervalsRefFilter : BoolExprFn
  {
    bool apply (Expr e)
    {
      Expr lhs = e->left();
      Expr rhs = e->right();

      return ((isOpX<GEQ>(e) || isOpX<LEQ>(e) 
	       || isOpX<GT>(e) || isOpX<LT>(e) 
	       || isOpX<EQ>(e) || isOpX<NEQ>(e)) 
	      && ((isOpX<MPQ>(lhs) && isOpX<VALUE>(rhs)) || 
		  (isOpX<MPQ>(rhs) && isOpX<VALUE>(lhs))));
      
    }
  };

  class ClausalPADomain : public PredicateAbstractDomain
  {
  public:
    ClausalPADomain (LBE& l, ExprFactory &e, ExprMSat& m) :
      PredicateAbstractDomain (l, e, m) {}
    
    std::string name () { return "CAPRED"; }
    
    virtual Expr alphaPost (Expr pre, SEdgePtr edge)
    {
      Environment env (efac);
      // -- predicate abstraction query
      Expr paq = SEdgeCondComp::computeEdgeCond (efac, env, pre, edge, lbe);

      ExprSet &dstPreds = getPreds (edge->getDst ());

      if (dstPreds.empty ())
	{
	  boost::tribool sat = isSAT (paq);
	  if (sat) return trueE;
          else if (!sat) return falseE;
          else // maybe
	    {
	      assert(0 && "\nMAYBE\n");
	      return trueE;
	    }
	}
        

      UfoZ3 z3 (efac);
      ZSolver<UfoZ3> smt (z3);
      smt.assertExpr (paq);
      

      // -- Boolean predicates
      ExprVector bpreds;
      bpreds.reserve (dstPreds.size ());
      
      // -- generate predicate definitions and add them to the solver
      forall (const Expr &p, dstPreds)
	{
	  Expr pred = bind::boolVar (p);
          bpreds.push_back (pred);
          smt.assertExpr (mk<IFF> (env.eval (p), pred));
        }
      
      typedef std::list<Expr> ExprList;
      
      ExprList models;
      ExprSet antiModels;
      
      tribool sat;
      while ((sat = smt.solve ()))
        {
          LOG("clpred", 
              dbgs () << "."; dbgs ().flush ());
          
          // -- compute current model
          ZModel<UfoZ3> zm = smt.getModel ();
          ExprVector modelVec;
          forall (const Expr &b, bpreds)
            {
              Expr val = zm.eval (b);
              if (isOpX<TRUE> (val)) modelVec.push_back (b);
              else if (isOpX<FALSE> (val)) modelVec.push_back (mk<NEG> (b));
            }
          Expr model = mknary<AND> (mk<TRUE> (efac), modelVec);
          models.push_back (model);

          // -- block current model
          smt.assertExpr (mk<NEG> (model));

          // -- look for an anti-model
          Expr antiModel = findAntiModel (smt, modelVec);
          // -- if a non-trivial anti-model found, store it
          if (!isOpX<FALSE> (antiModel)) 
            {
              if (antiModels.insert (antiModel).second)
                {
                  smt.assertExpr (mk<NEG> (antiModel));
                  LOG ("clpred", dbgs () << "+"; dbgs ().flush (););
                }
              
              LOG("clpred_verbose", 
                  dbgs () << "antimodel: " << *antiModel << "\n");
            }
        }
      // -- no unknowns
      UFO_ASSERT (sat || !sat);
      

      // -- remove all models that are implied by the negation of the
      // -- antimodels

      // -- negate and push negation in to get clauses
      Expr res = boolop::nnf 
        (boolop::lneg (mknary<OR> (mk<TRUE> (efac), antiModels)));

      // -- check what models anti-models do not cover. 
      // -- new solver
      smt.reset ();
      smt.assertExpr (mknary<OR> (mk<TRUE> (efac), antiModels));
      for (ExprList::iterator it = models.begin (); it != models.end ();)
        {
          // XXX Using SMT solver seems like an over-kill here. 
          smt.push ();
          const Expr &m = *it;
          smt.assertExpr (m);
          sat = smt.solve ();
          smt.pop ();
          
          UFO_ASSERT (sat || !sat);
          if (!sat) 
            {
              it = models.erase (it);
              LOG ("clpred", dbgs () << "x"; dbgs ().flush (););
            }
          else 
            {
              ++it;
              LOG ("clpred", dbgs () << "o"; dbgs ().flush (););
            }
          
        }
      LOG ("clpred", dbgs () << "\n";);

      // -- add models to current result
      if (!models.empty ())
        {
          models.push_back (res);
          res = mknary<OR> (mk<TRUE> (efac), models);
        }
      
      
      // -- unwrap the result to be over original destination predicates
      paasd::OverPredicates op;
      return dagVisit (op, res);
    }
    
  private:
    template <typename Range>
    Expr findAntiModel (ZSolver<UfoZ3> &smt, const Range &model)
    {
      ExprVector antiModel (boost::size (model));
      assert (!antiModel.empty ());
      
      boost::transform (model, antiModel.begin (), boolop::lneg);
      tribool sat = smt.solveAssuming (antiModel);
      UFO_ASSERT (sat || !sat);
      if (sat) return mk<FALSE> (efac);
      
      ExprVector core;
      smt.unsatCore (std::back_inserter (core));
      return mknary<AND> (mk<FALSE> (efac), core);
    }
  };  

  class QuantPADomain : public PredicateAbstractDomain
  {
  public:
    QuantPADomain (LBE& l, ExprFactory &e, ExprMSat& m) :
      PredicateAbstractDomain (l, e, m) {}
    
    std::string name () { return "QPRED"; }
    
    virtual Expr alphaPost (Expr pre, SEdgePtr edge)
    {
      Environment env (efac);
      // -- predicate abstraction query
      Expr paq = SEdgeCondComp::computeEdgeCond (efac, env, pre, edge, lbe);

      ExprSet &dstPreds = getPreds (edge->getDst ());

      if (dstPreds.empty ())
	{
	  boost::tribool sat = isSAT (paq);
	  if (sat) return trueE;
          else if (!sat) return falseE;
          else // maybe
	    {
	      assert(0 && "\nMAYBE\n");
	      return trueE;
	    }
	}      

      
      ExprVector vars;
      expr::filter (paq, IsVar(), std::back_inserter (vars));


      // -- Boolean predicates
      ExprVector bpreds;
      bpreds.reserve (dstPreds.size ());
      
      // -- generate predicate definitions and add them to the solver
      forall (const Expr &p, dstPreds)
	{
	  Expr pred = bind::boolVar (p);
          bpreds.push_back (pred);
	  paq = mk<AND> (paq, mk<IFF> (env.eval (p), pred));
        }
      
      UfoZ3 z3 (efac);
      ZSolver<UfoZ3> smt (z3);
      smt.assertForallExpr (vars, mk<NEG> (paq));
      
      ZSolver<UfoZ3> osmt (z3);
      osmt.assertExpr (paq);      

      LOG("qpred", (dbgs () << "QPRED:allSat:").flush (););

      Expr res = mk<TRUE> (efac);
      
      tribool sat;
      while ((sat = smt.solve ()))
	{
	  LOG ("qpred", (dbgs () << "/").flush (););
	  UFO_ASSERT (sat || !sat);
	  ExprVector model;
	  ZModel<UfoZ3> zm = smt.getModel ();
	  forall (const Expr &b, bpreds)
	    {
	      Expr val = zm.eval (b);
	      if (isOpX<TRUE> (val)) model.push_back (b);
	      else if (isOpX<FALSE> (val)) model.push_back (mk<NEG> (b));
	    }
	  unsigned sz = model.size ();
	  tribool osat = osmt.solveAssuming (model);
	  UFO_ASSERT (!osat);
	  model.clear ();
	  osmt.unsatCore (std::back_inserter (model));
	  if (model.size () < sz) LOG ("qpred", (dbgs () << "\\").flush (););
	  Expr cube = mknary<AND> (mk<FALSE> (efac), model);
	  smt.assertExpr (mk<NEG> (cube));
	  res = boolop::land (res, mk<NEG> (cube));
	}
      UFO_ASSERT (sat || !sat);
      LOG ("qpred", (dbgs () << "\n").flush (););
      
      res = boolop::nnf (res);
      

      osmt.assertExpr (mk<NEG> (res));
      UFO_ASSERT (!osmt.solve ());


      // -- unwrap the result to be over original destination predicates
      paasd::OverPredicates op;
      return dagVisit (op, res);
    }
  };
}

#endif
