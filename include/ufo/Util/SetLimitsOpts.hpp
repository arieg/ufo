#ifndef _SET_LIMITS_OPTS_H_
#define _SET_LIMITS_OPTS_H_

namespace ufo
{
  extern unsigned ufoCpuLimit;
  extern unsigned ufoMemLimit;
}

#endif
