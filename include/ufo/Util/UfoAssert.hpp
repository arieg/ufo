#undef UFO_ASSERT

#if defined(UFO_DISABLE_ASSERTS) || defined(NDEBUG)
# define UFO_ASSERT(a) ((void)0)
#else
# define UFO_ASSERT(a) ((a)                                            \
                         ? ((void)0)                                    \
                         : ufo::assertion_failed(#a, __FILE__, __LINE__))
#endif

#undef UFO_VERIFY

#if defined(UFO_DISABLE_ASSERTS) || defined(NDEBUG)

#define UFO_VERIFY(a) ((void)a)

#else

#define UFO_VERIFY(a) UFO_ASSERT(a)

#endif 


#ifndef UFO_ASSERT_H_
#define UFO_ASSERT_H_

#include <cstdlib>
#include "llvm/Support/raw_ostream.h"

namespace ufo
{
  inline void assertion_failed (char const *expr, char const * file, long line)
  {
    llvm::errs () << "Error:" << file << ":" << line << ":" 
                  << " Assertion: " << expr << "\n";
    llvm::errs ().flush ();
    std::abort ();
  }
  
}

#endif

