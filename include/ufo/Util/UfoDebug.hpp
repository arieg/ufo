#ifndef __UFO__DEBUG__HPP_
#define __UFO__DEBUG__HPP_
#include "llvm/Support/Debug.h"
#include <string>
#include <set>


namespace ufo
{
  void validateUfoResult (bool res);

#ifndef NUFOLOG
#define LOG(TAG,CODE)							\
  do { if (::ufo::UfoLogFlag && ::ufo::UfoLog.count (TAG) > 0) { CODE; } \
  } while (0)

  extern bool UfoLogFlag;
  extern std::set<std::string> UfoLog;
  
  void UfoEnableTrace (std::string x);
#else
#define UfoEnableTrace(X)
#define LOG(TAG,CODE) do { } while (0)
#endif
}


#endif
