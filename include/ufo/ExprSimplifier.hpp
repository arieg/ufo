#ifndef _EXPR_SIMPLIFIER_H_
#define _EXPR_SIMPLIFIER_H_

#include "Expr.hpp"


/** Old expression simplifier code. To be phased out */

namespace ufo
{

  struct PropSimplifier
  {
    Expr trueE;
    Expr falseE;
    ExprFactory& efac;

    PropSimplifier(Expr t, Expr f, ExprFactory& efaci) : 
      trueE(t), falseE(f), efac(efaci) {}

    Expr operator() (Expr exp) 
    { 

      if (isOpX<IMPL>(exp))
      {
        // TRUE -> x  == x
        if (trueE == exp->left ()) return exp->right();

        // FALSE -> x == TRUE
        if (falseE == exp->left ()) return trueE;

        // x -> TRUE == TRUE
        if (trueE == exp->right ()) return trueE;

        return exp;
      }

      if (isOpX<NEG>(exp))
      {
        if (trueE == exp->left()) return falseE;
        if (falseE == exp->left()) return trueE;
        if (isOpX<NEG> (exp->left ())) return exp->left ()->left ();
        if (isOpX<AND>(exp->left()))
        {
          Expr e = exp->left();
          std::vector<Expr> args;
          for (ENode::args_iterator it = e->args_begin (), 
              end = e->args_end (); it != end; ++it){
            if (isOpX<NEG>(*it))
              args.push_back((*it)->left());
            else
              args.push_back(mk<NEG>(*it));
          }
          assert(args.size() != 1);
          return mknary<OR>(args.begin(), args.end());
        }
        if (isOpX<OR>(exp->left()))
        {
          Expr e = exp->left();
          std::vector<Expr> args;
          for (ENode::args_iterator it = e->args_begin (), 
              end = e->args_end (); it != end; ++it){
            if (isOpX<NEG>(*it))
              args.push_back((*it)->left());
            else
              args.push_back(mk<NEG>(*it));
          }
          assert(args.size() != 1);
          return mknary<AND>(args.begin(), args.end());
        }

        return exp;
      }

      if (isOpX<OR>(exp))
      {
        // -- special case
        if (exp->arity() == 1) return exp->left();

        if (exp->arity () == 2)
        {
          Expr lhs = exp->left ();
          Expr rhs = exp->right ();

          if (lhs == rhs) return lhs;
          if (trueE == lhs || trueE == rhs) return trueE;
          if (falseE == lhs) return rhs;
          if (falseE == rhs) return lhs;
          // (!a || a)
          if (isOpX<NEG>(lhs) && lhs->left () == rhs) return trueE;
          // (a || !a)
          if (isOpX<NEG>(rhs) && rhs->left () == lhs) return trueE;
          if (! (isOpX<OR>(lhs) || isOpX<OR>(rhs))) return exp;
        }

        // XXX The next part agregates binary operators into an
        // n-ary operator for readability
        std::set<Expr> newArgs;
        for (ENode::args_iterator it = exp->args_begin (), 
            end = exp->args_end (); it != end; ++it)
        {
          Expr arg = *it;
          if (! isOpX<OR> (arg)) 
          {
            if (arg == trueE) return trueE;
            if (arg != falseE) newArgs.insert (arg);
          }
          else
          {
            for (ENode::args_iterator argit = arg->args_begin (),
                argend = arg->args_end (); argit != argend; ++argit)
            {
              Expr t = *argit;
              if (t == trueE) return trueE;
              if (t != falseE) newArgs.insert (t);
            }
          }

        }
        if (newArgs.size () == 0)
          return falseE;
        if (newArgs.size () == 1)
          return *(newArgs.begin ());

        return mknary<OR> (newArgs.begin (), newArgs.end ());

      }

      if (isOpX<AND>(exp)){
        // -- special case
        if (exp->arity() == 1) return exp->left();

        if (exp->arity () == 2)
        {
          Expr lhs = exp->left ();
          Expr rhs = exp->right ();

          if (lhs == rhs) return lhs;
          if (falseE == lhs || falseE == rhs) return falseE;
          if (trueE == lhs) return rhs;
          if (trueE == rhs) return lhs;
          if (isOpX<NEG>(lhs) && lhs->left () == rhs) return falseE;
          if (isOpX<NEG>(rhs) && rhs->left () == lhs) return falseE;

          if (! (isOpX<AND>(lhs) || isOpX<AND>(rhs))) return exp;
        }

        // XXX Same comment as for OR

        std::set<Expr> newArgs;
        for (ENode::args_iterator it = exp->args_begin (), 
            end = exp->args_end (); it != end; ++it)
        {
          Expr arg = *it;
          if (! isOpX<AND> (arg)) 
          {
            if (arg == falseE) return falseE;
            if (arg != trueE) newArgs.insert (arg);
          }
          else
          {
            for (ENode::args_iterator argit = arg->args_begin (),
                argend = arg->args_end (); argit != argend; ++argit)
            {
              Expr t = *argit;
              if (t == falseE) return falseE;
              if (! (t == trueE)) newArgs.insert (t);
            }
          }

        }
        if (newArgs.size () == 0)
          return trueE;
        if (newArgs.size () == 1)
          return *(newArgs.begin ());

        return mknary<AND> (newArgs.begin (), newArgs.end ());
      }

      // XXX Cut off numeric simplifications (at least for now).
      return exp;

      if (exp->arity() == 2){
        if (isOpX<INT>(exp->left()) && isOpX<INT>(exp->right())){
          Expr op1 = exp->left();
          Expr op2 = exp->right();
          int i1 = boost::lexical_cast<int>(op1.get());
          int i2 = boost::lexical_cast<int>(op2.get());

          if (isOpX<PLUS>(exp))
            return (mkTerm(i1 + i2, efac));
          if (isOpX<MINUS>(exp))
            return (mkTerm(i1 - i2, efac));
          if (isOpX<EQ>(exp)){
            if (i1 == i2)
              return trueE;
            else return falseE;
          }
          if (isOpX<NEQ>(exp)){
            if (i1 != i2)
              return trueE;
            else return falseE;
          }
          if (isOpX<LEQ>(exp)){
            if (i1 <= i2)
              return trueE;
            else return falseE;
          }
          if (isOpX<GEQ>(exp)){
            if (i1 >= i2)
              return trueE;
            else return falseE;
          }
          if (isOpX<LT>(exp)){
            if (i1 < i2)
              return trueE;
            else return falseE;
          }
          if (isOpX<GT>(exp)){
            if (i1 > i2)
              return trueE;
            else return falseE;
          }
          else
            return exp;
        }
      }
      return exp;
    }
  };

  struct PropSimplifierNNF
  {
    Expr trueE;
    Expr falseE;
    ExprFactory& efac;

    PropSimplifierNNF(Expr t, Expr f, ExprFactory& efaci) : 
      trueE(t), falseE(f), efac(efaci) {}

    VisitAction operator() (Expr exp) 
    {
      if (! isOpX<NEG> (exp)) return VisitAction::doKids ();

      if (isOpX<TRUE>(exp->left()))
        return VisitAction::changeTo (falseE);
      if (isOpX<FALSE>(exp->left()))
        return VisitAction::changeTo (trueE);

      // !! x -> &x
      if (isOpX<NEG> (exp->left ()))
      {
        ExprVector args;
        args.push_back (exp->left()->left());
        // --unary AND
        return 
          VisitAction::changeDoKids(mknary<AND>(args.begin(), args.end()));
      }

      if (isOpX<AND>(exp->left())) 
      {
        Expr e = exp->left ();
        ExprVector args;
        for (ENode::args_iterator it = e->args_begin (), 
            end = e->args_end (); it != end; ++it)
          args.push_back (boolop::lneg (*it));
        return VisitAction::changeDoKids(mknary<OR>(args.begin(), 
              args.end()));
      }

      if (isOpX<OR>(exp->left()))
      {
        Expr e = exp->left();
        ExprVector args;
        for (ENode::args_iterator it = e->args_begin (), 
            end = e->args_end (); it != end; ++it)
          args.push_back (boolop::lneg (*it));
        return VisitAction::changeDoKids(mknary<AND>(args.begin(),
              args.end()));
      }

      return VisitAction::doKids ();
    }
  };

  struct Simp
  {
    ExprFactory& efac;
    Expr trueE;
    Expr falseE;

    boost::shared_ptr<PropSimplifier> ps;

    Simp(Expr t, Expr f, ExprFactory& efaci) : 
      efac(efaci), trueE(t), falseE(f), ps(new PropSimplifier (t,f,efaci)) {}

    VisitAction operator() (Expr exp)
    {
      return VisitAction::changeDoKidsRewrite (exp, ps);
    }
  };


  
  inline Expr exprToNNF(Expr e)
  {
    ExprFactory& efac = e->efac();
    PropSimplifierNNF nnf(mk<TRUE>(efac), mk<FALSE>(efac), efac);
    Simp simp(mk<TRUE>(efac), mk<FALSE>(efac), efac);
    Expr e1 = dagVisit(nnf, e);
    Expr e2 = dagVisit(simp, e1);
    return e2;
  }
}



#endif /* _EXPRSIMPLIFIER_H_ */
