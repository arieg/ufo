#include "llvm/Support/Signals.h"
#include "llvm/Support/Debug.h"
#include "llvm/Support/PrettyStackTrace.h"
#include "llvm/Support/SystemUtils.h"
#include "llvm/Support/CommandLine.h"
#include "llvm/Support/ManagedStatic.h"
#include "llvm/Support/raw_ostream.h"

#include "ufo/Smt/UfoZ3.hpp"
#include "ufo/ufo.hpp"
#include "ufo/Smt/ExprZ3.hpp"


#include <fstream>

using namespace llvm;
using namespace ufo;

static cl::opt<std::string> 
BenchmarkFilename ("b",
		  cl::desc("Benchmark filename"),
		  cl::value_desc("filename"));

namespace
{
  struct SymbaApp
  {
    ExprFactory efac;
    
    SymbaApp () {}

    void run ()
    {
      errs () << "Loading smt file: " << BenchmarkFilename << "\n";
      std::string benchStr = loadFile (BenchmarkFilename);

      errs () << "Parsing...\n";
      Expr fmla = z3_from_smtlib2 (efac, benchStr);
      
      errs () << "Got formula:\n"
	      << *fmla << "\n";

      errs () << "Back to SMT-LIB2:\n"
	      << z3_to_smtlib2_with_decl (fmla) 
	      << "\n";
    }

    std::string loadFile (std::string fname)
    {
      // -- open file
      std::ifstream f(fname.c_str ());
    
      // -- compute size
      f.seekg(0, std::ios::end);
      size_t size = f.tellg();
      f.seekg(0);
    
      std::string buffer (size, ' ');
      
      f.read (&buffer[0], size);
      return buffer;
    }
    
    
  };
    
}


int main (int argc, char** argv)
{
  sys::PrintStackTraceOnErrorSignal ();
  llvm::PrettyStackTraceProgram X (argc, argv);
  EnableDebugBuffering = true;
  llvm_shutdown_obj Y;
  
  cl::ParseCommandLineOptions (argc, argv, 
			       "symba : "
			       "Symbolic Abstraction Benchmark Runner\n");
  
  if (!BenchmarkFilename.empty ())
    {
      SymbaApp symba;
      symba.run ();
    }
}
