# Find MathSat

find_package (Gmp QUIET)

set(MATHSAT_ROOT "" CACHE PATH "Root of MathSat distribution.")
find_path(MATHSAT_INCLUDE_DIR NAMES mathsat.h PATHS ${MATHSAT_ROOT}/include)
find_library(MATHSAT_LIBRARY NAMES mathsat PATHS ${MATHSAT_ROOT}/lib)

find_program (MATHSAT_EXECUTABLE
  NAMES mathsat PATHS ${MATHSAT_ROOT} PATH_SUFFIXES bin 
  DOC "mathsat command line executable")
mark_as_advanced(MATHSAT_EXECUTABLE)

if (MATHSAT_EXECUTABLE)
  execute_process (COMMAND ${MATHSAT_EXECUTABLE} -version
    OUTPUT_VARIABLE mathsat_version
    ERROR_QUIET
    OUTPUT_STRIP_TRAILING_WHITESPACE)
  if (mathsat_version MATCHES "^MathSAT5 version [0-9]")
    string (REGEX REPLACE "MathSAT5 version ([0-9.]+)[ ].*" "\\1" MATHSAT_VERSION_STRING ${mathsat_version})
  endif()
endif()


include (FindPackageHandleStandardArgs)
find_package_handle_standard_args(MathSAT
  REQUIRED_VARS MATHSAT_LIBRARY MATHSAT_INCLUDE_DIR GMP_FOUND
  VERSION_VAR MATHSAT_VERSION_STRING 
  )

mark_as_advanced(MATHSAT_VERSION_STRING MATHSAT_LIBRARY 
  MATHSAT_INCLUDE_DIR MATHSAT_EXECUTABLE)