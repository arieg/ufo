set(STP_ROOT "" CACHE PATH "Root of STP compiled source tree.")

find_path(STP_INCLUDE_DIR NAMES stp/c_interface.h PATHS ${STP_ROOT}/include)
find_library(STP_LIBRARY NAMES stp PATHS ${STP_ROOT}/lib)


include (FindPackageHandleStandardArgs)
find_package_handle_standard_args(STP
  REQUIRED_VARS STP_LIBRARY STP_INCLUDE_DIR)

mark_as_advanced(STP_LIBRARY STP_INCLUDE_DIR)